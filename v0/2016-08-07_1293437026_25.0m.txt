Split,Swim Stroke,Lengths,Distance,Time,Avg Pace,Best Pace,Avg SWOLF,Avg Power,Max Power,Total Strokes,Avg Strokes,Calories
1,MIXED,4,100.0,00:01:42.789,0:17:04  ,0:12:22  ,37.0,--,--,47,11.0,22
1.1,BUTTERFLY,1,25.0,00:00:23.00,0:15:20  ,0:15:20  ,34.0,--,--,11,--,--
1.2,BACKSTROKE,1,25.0,00:00:28.00,0:18:41  ,0:18:41  ,40.0,--,--,12,--,--
1.3,BREASTSTROKE,1,25.0,00:00:32.875,0:21:55  ,0:21:55  ,47.0,--,--,14,--,--
1.4,FREESTYLE,1,25.0,00:00:18.562,0:12:22  ,0:12:22  ,29.0,--,--,10,--,--
REST,,,0.0,00:02:28.641,--,--,0.0,--,--,--,11.75,--
2,BUTTERFLY,1,25.0,00:00:20.789,0:13:52  ,0:13:52  ,30.0,--,--,9,9.0,6
2.1,BUTTERFLY,1,25.0,00:00:20.812,0:13:52  ,0:13:52  ,30.0,--,--,9,--,--
REST,,,0.0,00:00:26.898,--,--,0.0,--,--,--,10.666666666666666,--
3,FREESTYLE,4,100.0,00:01:41.953,0:17:01  ,0:16:17  ,36.0,--,--,45,11.0,20
3.1,FREESTYLE,1,25.0,00:00:25.375,0:16:55  ,0:16:55  ,33.0,--,--,8,--,--
3.2,FREESTYLE,1,25.0,00:00:24.437,0:16:17  ,0:16:17  ,35.0,--,--,11,--,--
3.3,FREESTYLE,1,25.0,00:00:26.562,0:17:42  ,0:17:42  ,40.0,--,--,13,--,--
3.4,FREESTYLE,1,25.0,00:00:25.812,0:17:13  ,0:17:13  ,39.0,--,--,13,--,--
REST,,,0.0,00:00:12.47,--,--,0.0,--,--,--,11.222222222222221,--
4,BREASTSTROKE,1,25.0,00:00:34.563,0:23:03  ,0:23:03  ,51.0,--,--,16,16.0,6
4.1,BREASTSTROKE,1,25.0,00:00:34.562,0:23:03  ,0:23:03  ,51.0,--,--,16,--,--
REST,,,0.0,00:01:28.813,--,--,0.0,--,--,--,11.363636363636363,--
5,FREESTYLE,4,100.0,00:01:37.836,0:16:18  ,0:15:05  ,34.0,--,--,38,9.0,20
5.1,FREESTYLE,1,25.0,00:00:22.625,0:15:05  ,0:15:05  ,31.0,--,--,8,--,--
5.2,FREESTYLE,1,25.0,00:00:23.812,0:15:53  ,0:15:53  ,36.0,--,--,12,--,--
5.3,FREESTYLE,1,25.0,00:00:24.437,0:16:17  ,0:16:17  ,32.0,--,--,8,--,--
5.4,FREESTYLE,1,25.0,00:00:27.00,0:18:01  ,0:18:01  ,37.0,--,--,10,--,--
REST,,,0.0,00:00:09.852,--,--,0.0,--,--,--,11.466666666666667,--
6,BREASTSTROKE,2,50.0,00:01:10.945,0:23:38  ,0:23:03  ,52.0,--,--,33,16.0,12
6.1,BREASTSTROKE,1,25.0,00:00:36.375,0:24:15  ,0:24:15  ,53.0,--,--,17,--,--
6.2,BREASTSTROKE,1,25.0,00:00:34.562,0:23:03  ,0:23:03  ,51.0,--,--,16,--,--
REST,,,0.0,00:00:18.117,--,--,0.0,--,--,--,11.75,--
7,BUTTERFLY,1,25.0,00:00:22.117,0:14:47  ,0:14:48  ,31.0,--,--,9,9.0,6
7.1,BUTTERFLY,1,25.0,00:00:22.187,0:14:48  ,0:14:48  ,31.0,--,--,9,--,--
REST,,,0.0,00:00:08.758,--,--,0.0,--,--,--,11.588235294117647,--
8,BREASTSTROKE,1,25.0,00:00:33.813,0:22:33  ,0:22:33  ,50.0,--,--,16,16.0,6
8.1,BREASTSTROKE,1,25.0,00:00:33.812,0:22:33  ,0:22:33  ,50.0,--,--,16,--,--
REST,,,0.0,00:00:08.813,--,--,0.0,--,--,--,11.736842105263158,--
9,FREESTYLE,2,50.0,00:00:44.945,0:14:58  ,0:14:20  ,33.0,--,--,22,11.0,10
9.1,FREESTYLE,1,25.0,00:00:21.500,0:14:20  ,0:14:20  ,32.0,--,--,10,--,--
9.2,FREESTYLE,1,25.0,00:00:23.437,0:15:38  ,0:15:38  ,35.0,--,--,12,--,--
Summary,,20,500.0,00:14:11.867,0:17:39  ,0:12:22  ,38.0,--,--,235,11.699999809265137,108
