Split,Swim Stroke,Lengths,Distance,Time,Avg Pace,Best Pace,Avg SWOLF,Avg Power,Max Power,Total Strokes,Avg Strokes,Calories
1,FREESTYLE,4,200.0,00:03:06.320,0:15:31  ,0:14:45  ,72.0,--,--,102,25.0,36
1.1,FREESTYLE,1,50.0,00:00:44.375,0:14:48  ,0:14:48  ,68.0,--,--,24,--,--
1.2,FREESTYLE,1,50.0,00:00:48.750,0:16:15  ,0:16:15  ,74.0,--,--,25,--,--
1.3,FREESTYLE,1,50.0,00:00:48.812,0:16:16  ,0:16:16  ,77.0,--,--,28,--,--
1.4,FREESTYLE,1,50.0,00:00:44.250,0:14:45  ,0:14:45  ,69.0,--,--,25,--,--
REST,,,0.0,00:00:52.539,--,--,0.0,--,--,--,25.6,--
2,FREESTYLE,4,200.0,00:03:45.805,0:18:48  ,0:18:12  ,81.0,--,--,101,25.0,36
2.1,FREESTYLE,1,50.0,00:00:56.875,0:18:57  ,0:18:57  ,83.0,--,--,26,--,--
2.2,FREESTYLE,1,50.0,00:00:56.00,0:18:41  ,0:18:41  ,81.0,--,--,25,--,--
2.3,FREESTYLE,1,50.0,00:00:58.312,0:19:26  ,0:19:26  ,83.0,--,--,25,--,--
2.4,FREESTYLE,1,50.0,00:00:54.625,0:18:12  ,0:18:12  ,80.0,--,--,25,--,--
REST,,,0.0,00:00:17.945,--,--,0.0,--,--,--,25.666666666666668,--
3,BREASTSTROKE,2,100.0,00:02:12.859,0:22:08  ,0:20:57  ,93.0,--,--,53,26.0,26
3.1,BREASTSTROKE,1,50.0,00:01:10.00,0:23:20  ,0:23:20  ,98.0,--,--,28,--,--
3.2,BREASTSTROKE,1,50.0,00:01:02.875,0:20:57  ,0:20:57  ,88.0,--,--,25,--,--
REST,,,0.0,00:02:46.766,--,--,0.0,--,--,--,25.454545454545453,--
4,FREESTYLE,4,200.0,00:03:27.188,0:17:16  ,0:16:23  ,77.0,--,--,104,26.0,36
4.1,FREESTYLE,1,50.0,00:00:49.125,0:16:23  ,0:16:23  ,73.0,--,--,24,--,--
4.2,FREESTYLE,1,50.0,00:00:50.500,0:16:50  ,0:16:50  ,76.0,--,--,25,--,--
4.3,FREESTYLE,1,50.0,00:00:57.437,0:19:09  ,0:19:09  ,86.0,--,--,29,--,--
4.4,FREESTYLE,1,50.0,00:00:50.187,0:16:44  ,0:16:44  ,76.0,--,--,26,--,--
REST,,,0.0,00:01:36.250,--,--,0.0,--,--,--,25.6,--
5,FREESTYLE,2,100.0,00:01:43.992,0:17:20  ,0:17:04  ,77.0,--,--,50,25.0,18
5.1,FREESTYLE,1,50.0,00:00:52.875,0:17:38  ,0:17:38  ,77.0,--,--,24,--,--
5.2,FREESTYLE,1,50.0,00:00:51.187,0:17:04  ,0:17:04  ,77.0,--,--,26,--,--
REST,,,0.0,00:00:07.695,--,--,0.0,--,--,--,26.11764705882353,--
6,BREASTSTROKE,2,100.0,00:02:20.969,0:23:30  ,0:21:22  ,101.0,--,--,62,31.0,25
6.1,BREASTSTROKE,1,50.0,00:01:16.937,0:25:40  ,0:25:40  ,111.0,--,--,34,--,--
6.2,BREASTSTROKE,1,50.0,00:01:04.62,0:21:22  ,0:21:22  ,92.0,--,--,28,--,--
REST,,,0.0,00:01:04.843,--,--,0.0,--,--,--,26.22222222222222,--
7,FREESTYLE,1,50.0,00:00:46.625,0:15:33  ,0:15:34  ,71.0,--,--,24,24.0,9
7.1,FREESTYLE,1,50.0,00:00:46.687,0:15:34  ,0:15:34  ,71.0,--,--,24,--,--
REST,,,0.0,00:03:14.500,--,--,0.0,--,--,--,26.105263157894736,--
8,FREESTYLE,1,50.0,00:00:45.422,0:15:07  ,0:15:08  ,68.0,--,--,23,23.0,9
8.1,FREESTYLE,1,50.0,00:00:45.375,0:15:08  ,0:15:08  ,68.0,--,--,23,--,--
REST,,,0.0,00:01:29.578,--,--,0.0,--,--,--,25.95,--
9,DRILL,1,50.0,00:00:45.680,--,--,0.0,--,--,--,24.714285714285715,--
9.1,DRILL,1,50.0,00:00:45.680,--,--,--,--,--,0,--,--
REST,,,0.0,00:04:18.632,--,--,0.0,--,--,--,24.714285714285715,--
10,FREESTYLE,1,50.0,00:00:40.383,0:10:41  ,0:10:41  ,50.0,--,--,18,18.0,9
10.1,FREESTYLE,1,50.0,00:00:32.61,0:10:41  ,0:10:41  ,50.0,--,--,18,--,--
Summary,,22,1100.0,00:35:24.328,0:17:48  ,0:10:41  ,78.0,--,--,537,25.5,204
