Split,Swim Stroke,Lengths,Distance,Time,Avg Pace,Best Pace,Avg SWOLF,Avg Power,Max Power,Total Strokes,Avg Strokes,Calories
1,BREASTSTROKE,2,100.0,00:02:10.390,0:21:42  ,0:20:40  ,87.0,--,--,77,31.666666666666668,26
1.1,BREASTSTROKE,1,50.0,00:01:08.125,0:22:44  ,0:22:44  ,109.0,--,--,41,--,--
1.2,BREASTSTROKE,1,50.0,00:01:02.00,0:20:40  ,0:20:40  ,98.0,--,--,36,--,--
REST,,,0.0,00:00:15.617,--,--,--,--,--,--,--,--
2,FREESTYLE,2,100.0,00:02:12.438,0:18:22  ,0:12:19  ,96.0,--,--,58,34.0,18
2.1,FREESTYLE,1,50.0,00:00:36.937,0:12:19  ,0:12:19  ,55.0,--,--,18,--,--
2.2,FREESTYLE,1,50.0,00:01:13.312,0:24:26  ,0:24:26  ,113.0,--,--,40,--,--
REST,,,0.0,00:00:06.750,--,--,--,--,--,--,--,--
3,FREESTYLE,8,400.0,00:07:41.969,0:19:14  ,0:14:22  ,92.0,--,--,245,31.875,72
3.1,FREESTYLE,1,50.0,00:00:50.312,0:16:47  ,0:16:47  ,78.0,--,--,28,--,--
3.2,FREESTYLE,1,50.0,00:00:43.125,0:14:22  ,0:14:22  ,67.0,--,--,24,--,--
3.3,FREESTYLE,1,50.0,00:01:12.875,0:24:17  ,0:24:17  ,111.0,--,--,38,--,--
3.4,FREESTYLE,1,50.0,00:00:59.562,0:19:51  ,0:19:51  ,92.0,--,--,32,--,--
3.5,FREESTYLE,1,50.0,00:00:56.937,0:18:58  ,0:18:58  ,87.0,--,--,30,--,--
3.6,FREESTYLE,1,50.0,00:01:02.250,0:20:45  ,0:20:45  ,94.0,--,--,32,--,--
3.7,FREESTYLE,1,50.0,00:00:59.375,0:19:47  ,0:19:47  ,90.0,--,--,31,--,--
3.8,FREESTYLE,1,50.0,00:00:57.500,0:19:10  ,0:19:10  ,88.0,--,--,30,--,--
REST,,,0.0,00:00:42.969,--,--,--,--,--,--,--,--
4,BREASTSTROKE,10,500.0,00:11:27.555,0:22:55  ,0:21:18  ,102.0,--,--,363,35.2,144
4.1,BREASTSTROKE,1,50.0,00:01:07.312,0:22:27  ,0:22:27  ,105.0,--,--,38,--,--
4.2,BREASTSTROKE,1,50.0,00:01:15.750,0:25:15  ,0:25:15  ,115.0,--,--,39,--,--
4.3,BREASTSTROKE,1,50.0,00:01:05.875,0:21:57  ,0:21:57  ,99.0,--,--,33,--,--
4.4,BREASTSTROKE,1,50.0,00:01:08.687,0:22:55  ,0:22:55  ,106.0,--,--,37,--,--
4.5,BREASTSTROKE,1,50.0,00:01:05.312,0:21:47  ,0:21:47  ,101.0,--,--,36,--,--
4.6,BREASTSTROKE,1,50.0,00:01:09.875,0:23:18  ,0:23:18  ,108.0,--,--,38,--,--
4.7,BREASTSTROKE,1,50.0,00:01:03.875,0:21:18  ,0:21:18  ,98.0,--,--,34,--,--
4.8,BREASTSTROKE,1,50.0,00:01:08.187,0:22:44  ,0:22:44  ,104.0,--,--,36,--,--
4.9,BREASTSTROKE,1,50.0,00:01:08.437,0:22:49  ,0:22:49  ,104.0,--,--,36,--,--
4.10,BREASTSTROKE,1,50.0,00:01:14.250,0:24:45  ,0:24:45  ,110.0,--,--,36,--,--
REST,,,0.0,00:04:32.820,--,--,--,--,--,--,--,--
5,FREESTYLE,8,400.0,00:07:41.383,0:19:13  ,0:16:53  ,92.0,--,--,239,31.25,72
5.1,FREESTYLE,1,50.0,00:00:50.625,0:16:53  ,0:16:53  ,78.0,--,--,27,--,--
5.2,FREESTYLE,1,50.0,00:00:54.187,0:18:04  ,0:18:04  ,81.0,--,--,27,--,--
5.3,FREESTYLE,1,50.0,00:00:58.250,0:19:25  ,0:19:25  ,88.0,--,--,30,--,--
5.4,FREESTYLE,1,50.0,00:00:58.875,0:19:37  ,0:19:37  ,90.0,--,--,31,--,--
5.5,FREESTYLE,1,50.0,00:00:59.187,0:19:44  ,0:19:44  ,89.0,--,--,30,--,--
5.6,FREESTYLE,1,50.0,00:01:02.125,0:20:43  ,0:20:43  ,93.0,--,--,31,--,--
5.7,FREESTYLE,1,50.0,00:00:58.375,0:19:28  ,0:19:28  ,89.0,--,--,31,--,--
5.8,FREESTYLE,1,50.0,00:00:59.750,0:19:56  ,0:19:56  ,92.0,--,--,32,--,--
REST,,,0.0,00:00:19.430,--,--,--,--,--,--,--,--
6,BREASTSTROKE,4,200.0,00:04:58.305,0:24:52  ,0:24:09  ,105.0,--,--,150,35.25,48
6.1,BREASTSTROKE,1,50.0,00:01:12.375,0:24:09  ,0:24:09  ,110.0,--,--,38,--,--
6.2,BREASTSTROKE,1,50.0,00:01:16.875,0:25:38  ,0:25:38  ,115.0,--,--,38,--,--
6.3,BREASTSTROKE,1,50.0,00:01:16.437,0:25:29  ,0:25:29  ,114.0,--,--,38,--,--
6.4,BREASTSTROKE,1,50.0,00:01:12.625,0:24:13  ,0:24:13  ,109.0,--,--,36,--,--
REST,,,0.0,00:00:54.383,--,--,--,--,--,--,--,--
7,FREESTYLE,2,100.0,00:01:45.875,0:17:38  ,0:17:26  ,84.0,--,--,58,29.5,18
7.1,FREESTYLE,1,50.0,00:00:53.625,0:17:52  ,0:17:52  ,83.0,--,--,29,--,--
7.2,FREESTYLE,1,50.0,00:00:52.250,0:17:26  ,0:17:26  ,81.0,--,--,29,--,--
REST,,,0.0,00:00:22.125,--,--,--,--,--,--,--,--
8,FREESTYLE,2,100.0,00:01:55.516,0:19:16  ,0:18:55  ,99.0,--,--,61,35.0,18
8.1,FREESTYLE,1,50.0,00:00:56.750,0:18:55  ,0:18:55  ,87.0,--,--,30,--,--
8.2,FREESTYLE,1,50.0,00:00:58.812,0:19:36  ,0:19:36  ,90.0,--,--,31,--,--
REST,,,0.0,00:00:18.922,--,--,--,--,--,--,--,--
9,BREASTSTROKE,2,100.0,00:02:24.23,0:24:00  ,0:22:40  ,101.0,--,--,78,35.5,25
9.1,BREASTSTROKE,1,50.0,00:01:08.00,0:22:40  ,0:22:40  ,107.0,--,--,39,--,--
9.2,BREASTSTROKE,1,50.0,00:01:16.62,0:25:22  ,0:25:22  ,115.0,--,--,39,--,--
REST,,,0.0,00:01:02.39,--,--,--,--,--,--,--,--
10,FREESTYLE,2,100.0,00:01:52.219,0:16:16  ,0:14:11  ,66.0,--,--,55,23.0,18
10.1,FREESTYLE,1,50.0,00:00:55.125,0:18:22  ,0:18:22  ,87.0,--,--,32,--,--
10.2,FREESTYLE,1,50.0,00:00:42.562,0:14:11  ,0:14:11  ,66.0,--,--,23,--,--
Summary,,42,2100.0,00:52:45.227,0:20:43  ,0:12:19  ,95.0,--,--,1384,32.95238095238095,459
