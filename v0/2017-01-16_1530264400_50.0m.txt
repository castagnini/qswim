Split,Swim Stroke,Lengths,Distance,Time,Avg Pace,Best Pace,Avg SWOLF,Avg Power,Max Power,Total Strokes,Avg Strokes,Calories
1,FREESTYLE,2,100.0,00:01:27.633,0:14:32  ,0:14:11  ,67.0,--,--,48,24.0,18
1.1,FREESTYLE,1,50.0,00:00:44.687,0:14:54  ,0:14:54  ,70.0,--,--,25,--,--
1.2,FREESTYLE,1,50.0,00:00:42.562,0:14:11  ,0:14:11  ,66.0,--,--,23,--,--
REST,,,0.0,00:00:03.500,--,--,0.0,--,--,--,26.666666666666668,--
2,BREASTSTROKE,2,100.0,00:02:12.664,0:18:01  ,0:12:41  ,78.0,--,--,48,24.0,27
2.1,BREASTSTROKE,1,50.0,00:01:10.00,0:23:20  ,0:23:20  ,102.0,--,--,32,--,--
2.2,BREASTSTROKE,1,50.0,00:00:38.62,0:12:41  ,0:12:41  ,54.0,--,--,16,--,--
REST,,,0.0,00:00:33.148,--,--,0.0,--,--,--,10.666666666666666,--
3,DRILL,6,300.0,00:05:20.328,--,--,0.0,--,--,--,9.6,--
3.1,DRILL,1,50.0,00:00:53.388,--,--,--,--,--,0,--,--
3.2,DRILL,1,50.0,00:00:53.388,--,--,--,--,--,0,--,--
3.3,DRILL,1,50.0,00:00:53.388,--,--,--,--,--,0,--,--
3.4,DRILL,1,50.0,00:00:53.388,--,--,--,--,--,0,--,--
3.5,DRILL,1,50.0,00:00:53.388,--,--,--,--,--,0,--,--
3.6,DRILL,1,50.0,00:00:53.388,--,--,--,--,--,0,--,--
Summary,,10,500.0,00:09:37.391,0:16:16  ,0:12:41  ,72.0,--,--,96,24.0,45
