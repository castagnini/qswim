

#ifndef _VOLUMEPLOT_H_
#define _VOLUMEPLOT_H_

#include <string>
#include <vector>
#include <memory>
#include "baseplot.h"


class VolumeGraph : public BaseGraph {
	public:
		VolumeGraph(): BaseGraph() { }
		const std::string graphLabel();

	std::string label;
};

class VolumePlot : public BasePlot {
	public:
		VolumePlot() ;
		const std::string plotTitle() ;
		const std::string xAxisLabel() ;
		const std::string yAxisLabel() ;

	void validatePreferences(PlotPreferences&);
	void createPlot(QCustomPlot* plot);
	double minX () {return ( graphs.size()>0 ? graphs[0]->minX : 0) ;}
	double maxX () {return ( graphs.size()>0 ? graphs[0]->maxX : 0) ;}
	double minY () {return ( graphs.size()>0 ? graphs[0]->minY : 0) ;}
	double maxY () {return ( graphs.size()>0 ? graphs[0]->maxY : 0) ;}
	volume_type_t quantity;
};


#endif


