
#include "testinput.h"
#include "smoothplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"

const std::string SmoothGraph::graphLabel(){
	return label;
}

const std::string SmoothPlot::plotTitle(){
	return std::string("Smooth"); 
}

const std::string SmoothPlot::xAxisLabel(){
	return std::string("pace min/100m"); 
}

const std::string SmoothPlot::yAxisLabel(){
	return std::string("2x strokes per min"); 
}

void SmoothPlot::validatePreferences(PlotPreferences&prefs){
	prefs.allow_period = true;
	prefs.allow_strokes = false;
	prefs.allow_pool = true;
	prefs.allow_interval = true;
	prefs.allow_points = true;
	prefs.allow_depth = false;	
	preferences = prefs;     
}

SmoothPlot::SmoothPlot(){
}

void SmoothPlot::createPlot(QCustomPlot* plot){
	std::vector<volume_type_t> quantities = {DISTANCE,TIME,STROKES};
	double pool = preferences.pool;	
	period_type_t period = preferences.period;	
	stroke_type_t stroke = FREESTYLE;	
	int interval = preferences.interval;
	int points = preferences.points;

	SwimVolumes sv (pool,period, interval, stroke,quantities);

	std::shared_ptr<SmoothGraph> smoothgraph = std::make_shared<SmoothGraph>();

	graphs.push_back(smoothgraph);

	if (points>0 && sv.x.size() > points){
		int to_erase = sv.x.size() - points;
		sv.x.erase(sv.x.begin(), sv.x.begin() + to_erase);
		sv.y[0].erase(sv.y[0].begin(), sv.y[0].begin() + to_erase);
		sv.y[1].erase(sv.y[1].begin(), sv.y[1].begin() + to_erase);
		sv.y[2].erase(sv.y[2].begin(), sv.y[2].begin() + to_erase);
	}

	smoothgraph->label = "" ;
	smoothgraph->x = sv.x;
	smoothgraph->y.resize(smoothgraph->x.size());
	for (unsigned long i = 0 ; i< smoothgraph->y.size(); ++i){
		double strk = sv.y[2][i];
		double t = sv.y[1][i]; 
		double d = sv.y[0][i]; 
		double stroke_per_min = 2.*strk*60./t ;
		double pace = 100.*t/d;
		smoothgraph->x[i] = pace;
		smoothgraph->y[i] = stroke_per_min;
		
	}
	// QCustomPlot needs a QVector.
	QVector<double> x = QVector<double>({60,180}) ;
	QVector<double> y = QVector<double>({120,120}) ;
	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(0)->setData(x, y);
	plot->graph(0)->setPen(QPen(QColor(0,0,0,0), 2));
	plot->graph(0)->setName("");
	x = QVector<double>(201) ;
	y = QVector<double>(201) ;
    for ( int i = 0 ; i< x.size(); i++) { 
		x[i] = 60. + i*(120.-60.)/200. ;
		y[i] = swimSmothUpper(x[i]);
	}
    plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(1)->setPen(QPen(QColor(230.,43.0,23.,255), 2));
	plot->graph(1)->setBrush(QColor(230.,43.0,23.,0.7*255));
    plot->graph(1)->setData(x, y);
    plot->graph(1)->setName("");	
 	//plot->graph(2)->setChannelFillGraph(plot->graph(1));
 	plot->graph(1)->setChannelFillGraph(plot->graph(0));
	x = QVector<double>(201) ;
	y = QVector<double>(201) ;
    for ( int i = 0 ; i< x.size(); i++) { 
		x[i] = 60. + i*(120.-60.)/200. ;
		y[i] = swimSmothLower(x[i]);
	}
    plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(2)->setPen(QPen(QColor(114.,151.0,230.,255), 2));
	plot->graph(2)->setBrush(QColor(114.,151.0,230.,0.7*255));
    plot->graph(2)->setData(x, y);
    plot->graph(2)->setName("");	


	auto graph = this->graphs[0];
	// QCustomPlot needs a QVector.
	x= QVector<double>::fromStdVector(graph->x);
	y= QVector<double>::fromStdVector(graph->y);
	QCPCurve * ratepace = new QCPCurve (plot->xAxis, plot->yAxis);
	ratepace->setData(x, y);
	ratepace->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	ratepace->setName(graph->graphLabel().c_str());


	plot->xAxis->setRangeReversed(true);
	plot->yAxis->setRangeReversed(false);

	plot->yAxis2->setVisible(false);

	// Creates data from test input
	double minX,minY,maxX,maxY;
	minX = 60;
	minY = 40;
	maxX = 120;
	maxY = 100;

    // calc extra padding for the plots
    	plot->legend->setVisible(false);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);

	QSharedPointer<QCPAxisTicker> normalTicker(new QCPAxisTicker);
  	plot->yAxis->setTicker(normalTicker);
  	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  	plot->xAxis->setTicker(timeTicker);
  
  	timeTicker->setTimeFormat("%m:%s");	
	
	plot->setInteraction(QCP::iRangeDrag, true);
	plot->setInteraction(QCP::iRangeZoom, true);
	
}






