
#include "testinput.h"
#include "bestintplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"
#include "levmar.h"

const std::string BestIntGraph::graphLabel(){
	return label;
}

const std::string BestIntPlot::plotTitle(){
	return std::string("BestInt"); 
}

const std::string BestIntPlot::xAxisLabel(){
	return std::string("interval length"); 
}

const std::string BestIntPlot::yAxisLabel(){
	return std::string("pace min/100m"); 
}

void BestIntPlot::validatePreferences(PlotPreferences&prefs){
	prefs.allow_period = false;
	prefs.allow_strokes = true;
	prefs.allow_pool = true;
	prefs.allow_interval = false;
	prefs.allow_points = false;
	prefs.allow_depth = false;	
	preferences = prefs;     
}

void BestIntPlot::createPlot(QCustomPlot* plot){
	double pool = preferences.pool;	
	period_type_t period = preferences.period;	
	stroke_type_t stroke = preferences.stroke;	

	period = EVER;
	std::shared_ptr<BestIntGraph> bestintgraph = std::make_shared<BestIntGraph>();
	graphs.push_back(bestintgraph);
	bestintgraph->x.clear();
	bestintgraph->y.clear();
    std::vector<int> intervals = {25,50,100,200,400,800,1500} ;
	for( int val: intervals){
		SwimVolumes sv (pool,period, val, stroke);
		if (sv.y.size()<1 || sv.y[0].size()<1) continue;
		bestintgraph->x.push_back((double)val);
		double t = sv.y[0].back();
		double pace = 100.*t/(double)val; 
		bestintgraph->y.push_back(pace);
	}
	if (bestintgraph->x.size() < 1 ) return;

	bestintgraph->label = "best over interval" ;
	bestintgraph->calcMinMax();

	auto graph = this->graphs[0];
	// QCustomPlot needs a QVector.
	QVector<double> x= QVector<double>::fromStdVector(graph->x);
	QVector<double> y= QVector<double>::fromStdVector(graph->y);
	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(0)->setData(x, y);
	plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	plot->graph(0)->setName(graph->graphLabel().c_str());


	plot->yAxis2->setVisible(false);

	// Creates data from test input
	double minX,minY,maxX,maxY;
	minX = this->minX();
	minY = this->minY();
	maxX = this->maxX();
	maxY = this->maxY();

	//fit 
	double par[5];
    int ret = levmar_fit(bestintgraph->x.size(),&bestintgraph->x[0],&bestintgraph->y[0],par);
	x = QVector<double>(201) ;
	y = QVector<double>(201) ;
    for ( int i = 0 ; i< x.size(); i++) { 
		x[i] = bestintgraph->x[0] + i*( bestintgraph->x.back() - bestintgraph->x[0])/200. ;
		double xx = x[i];
		y[i] = fit_func(par,0,&xx);
	}
    plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(1)->setPen(QPen(QColor(230.,43.0,23.,255), 2));
    plot->graph(1)->setData(x, y);
    plot->graph(1)->setName("fit");	




    // calc extra padding for the plots
    	plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());
	plot->xAxis->setRangeReversed(false);
	plot->yAxis->setRangeReversed(true);

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);

	QSharedPointer<QCPAxisTicker> normalTicker(new QCPAxisTicker);
  	plot->xAxis->setTicker(normalTicker);
  	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  	plot->yAxis->setTicker(timeTicker);
  
  	timeTicker->setTimeFormat("%m:%s");	
	
	plot->setInteraction(QCP::iRangeDrag, true);
	plot->setInteraction(QCP::iRangeZoom, true);
	
}






