
#ifndef DISTRO_VOL_H
#define DISTRO_VOL_H


#include <map>
#include <vector>

#include "activity.h"
#include "common_types.h"

class DistroVol {
	public:
	DistroVol ( double pool_len , period_type_t p, int interval_len,  stroke_type_t style, std::vector<volume_type_t> & quantities  );
	
	std::vector<double> x;
	std::vector<std::vector<double>> y;
};

#endif 

