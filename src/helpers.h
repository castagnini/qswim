
#ifndef _HELPERS_H_
#define  _HELPERS_H_

#include <cmath>

static inline double rescale_t_fit (double x){
	double par[] = {108.402781, -9529.252187, 0.000312, 801825.671687, -23.885577};

    return (par[0] + par[1]/(x + fabs(par[2]))  + par[3]/pow(x + fabs(par[4]),2  ));


//    return (1.10263665  - sqrt(17.8263103/ (x+   56.81665232)));
}

static inline double rescale_time(double time, double distance){
    
    double t_rescaled = time * rescale_t_fit (100.) /   rescale_t_fit (distance);
 //   printf("rescaled %lf %lf %lf \n",time , t_rescaled, distance);
    return t_rescaled;
}

static inline double swimSmothUpper(double x){
    
    return (  -5.65855505e+03 +
            4.19484724e+02 *x
            -1.30428440e+01 *x*x +
            2.26094793e-01*x*x*x
            -2.36823931e-03 *x*x*x*x
            + 1.49662779e-05 *x*x*x*x*x
            -5.26519865e-08 *x*x*x*x*x*x
            +  7.92495050e-11 *x*x*x*x*x*x*x );
}

static inline double swimSmothLower(double x){
    
    return( -3.37306858e+03  +
           2.66412383E+02 *x
           -8.61201726e+00 *x*x +
           1.51331590e-01 *x*x*x
           -1.56734733e-03  *x*x*x*x +
           9.58648092e-06 *x*x*x*x*x
           -3.20993111e-08  *x*x*x*x*x*x+
           4.54276051e-11 *x*x*x*x*x*x*x );
    
}


#endif

