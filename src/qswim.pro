#-------------------------------------------------
#
# Project created by QtCreator 2016-03-20T11:15:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = qswim
TEMPLATE = app
CONFIG += c++11
QMAKE_CFLAGS += -std=c99

SOURCES += main.cpp\
        mainwindow.cpp \
        qcustomplot.cpp \
        swimlen.cpp \
		parse_csv_v1.cpp \
        activity.cpp \
        swimvolumes.cpp \
        volumeplot.cpp \
	    restplot.cpp \
	    smoothplot.cpp \
        bestintplot.cpp \
        rescaledplot.cpp \
        bestplot.cpp \
        levmar.c \
        strokesplot.cpp \
        paceplot.cpp

HEADERS  += mainwindow.h \
           qcustomplot.h \
			bestintplot.h \
			bestplot.h \
			rescaledplot.h \
			paceplot.h \
			strokesplot.h \
			parse_csv_v1.h \
			swimlen.h \
            swimvolumes.h \
            smoothplot.h \
            restplot.h \
            volumeplot.h \
			levmar.h \
			activity.h \
			baseplot.h 




