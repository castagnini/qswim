#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QToolBox>
#include <QTreeWidget>
#include "qcustomplot.h"
#include "baseplot.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void InitPlot(int );
    ~MainWindow();
    void addConstToPlot(double c,double start, double end,Qt::GlobalColor color = Qt::red);

public slots:
	void itemCollapsed(QTreeWidgetItem *item);
	void currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *);

private:
    QToolBox *toolbox ;
    QTreeWidget *tree ;
    QTreeWidget *activitytree ;

	int current_plot_idx;

	QPushButton *m_quit, *m_save;
    QPushButton *m_button;

    QCustomPlot *plot;

	// Preferences Widgets
	QLabel * pools_label;
	QLabel * styles_label;
	QLabel * period_label;
	QLabel * interval_label;
	QLabel * points_label;
	QLabel * depth_label; 

	QComboBox * pools_combo;
	QComboBox * styles_combo;
	QComboBox * period_combo;
	QComboBox * interval_combo;
	QComboBox * points_combo;
	QComboBox * depth_combo; 
	
	PlotPreferences plotprefs;
	void showHidePrefCombo();
	void refreshPreferences();
};

#endif // MAINWINDOW_H
