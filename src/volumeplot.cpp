
#include "volumeplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"

const std::string VolumeGraph::graphLabel(){
    return label;
}

const std::string VolumePlot::plotTitle(){
    std::vector <std::string> labels= {"Distance", "Time", "Strokes", "Calories"};

    return labels[quantity];
}

const std::string VolumePlot::xAxisLabel(){
    return std::string("date");
}

const std::string VolumePlot::yAxisLabel(){

    std::vector <std::string> labels= {"meters", "min:sec", "", "kcal"};

    return labels[quantity];
}

void VolumePlot::validatePreferences(PlotPreferences&prefs){
    prefs.allow_period = true;
    prefs.allow_strokes = true;
    prefs.allow_pool = true;
    prefs.allow_interval = true;
    prefs.allow_points = false;
    prefs.allow_depth = false;
    preferences = prefs;
}

VolumePlot::VolumePlot(){
    // default
    quantity = DISTANCE;

}

void VolumePlot::createPlot(QCustomPlot* plot){
    std::vector<volume_type_t> quantities = {quantity};
    double pool = preferences.pool;
    period_type_t period = preferences.period;
    stroke_type_t stroke = preferences.stroke;
    int interval = preferences.interval;

    std::vector <std::string> stroke_labels = {"Free","Fly","Breast","Back", "Drill","IM"};
    std::vector<QColor> stroke_colors = {
        QColor(114.,151.0,230.,0.9*255),  // blue
        QColor(230.,43.0,23.,0.9*255),  // red
        QColor(17.,173.0,52.,0.9*255), // green
        QColor(235.,171.,33.,0.9*255), //yellow
        QColor(130.,130.0,130.,0.9*255), // gray
        QColor(189.,76.0,181.,0.9*255) // purple
    };
    std::vector<stroke_type_t> strokes_vec;
    if(stroke == ALL){
        for(int strk = FREESTYLE; strk<MIXED; ++strk){
            stroke_type_t tmp = (stroke_type_t)strk;
            strokes_vec.push_back(tmp);
        }
    }else{ strokes_vec.push_back(stroke);}

    std::vector<QCPBars *> bars_ptrs;

    for (auto strk : strokes_vec)
    {
        SwimVolumes sv (pool,period, interval, strk,quantities);
        if( sv.x.size() < 1) continue;
        std::shared_ptr<VolumeGraph> volgraph = std::make_shared<VolumeGraph>();
        graphs.push_back(volgraph);

        volgraph->label = stroke_labels[strk] ;
        volgraph->x = sv.x;
        volgraph->y = sv.y[0];

        // QCustomPlot needs a QVector.
        QVector<double> x= QVector<double>::fromStdVector(volgraph->x);
        QVector<double> y= QVector<double>::fromStdVector(volgraph->y);

        QCPBars *regen = new QCPBars(plot->xAxis, plot->yAxis);
        bars_ptrs.push_back(regen);
        regen->setAntialiased(false); // gives more crisp, pixel aligned bar borders
        regen->setStackingGap(1);
        regen->setName(stroke_labels[strk].c_str());
        regen->setPen(QPen(stroke_colors[strk].lighter(130)));
        regen->setBrush(stroke_colors[strk]);
        double w[] = {1,7,28,365};
        regen->setWidth(w[period]*3600*24*0.99);
        for(auto&px:x) px+=(w[period]*3600*24/2) ;
        regen->setData(x, y);
    }

    for(unsigned int i = 1; i<bars_ptrs.size(); ++i)
        bars_ptrs[i]->moveAbove(bars_ptrs[i-1]);

    // Creates data from test input
    double minX,minY,maxX,maxY;

    std::map<double,double> gsum;
    for (unsigned int i = 0; i< graphs.size(); i++){
        double px, py;
        for (unsigned int j = 0; j< graphs[i]->x.size(); j++){
            px = graphs[i]->x[j];
            py = graphs[i]->y[j];
            gsum[px] += py;
        }
    }

    if(graphs.size() > 0){
        minY = 0;// graphs[0]->y[0];
        minX = graphs[0]->x[0];;
        maxY = graphs[0]->y[0];;
        maxX = minX;
        for (auto & p : gsum)
        {
            double px, py;
            std::tie(px,py) = p;
            // finds max and min for the plot ranges
            minX = minX<px?minX:px;
            // minY = minY<py?minY:py;
            maxX = maxX>px?maxX:px;
            maxY = maxY>py?maxY:py;
        }
        double delta = maxX - minX;
        minX -= delta*0.05;
        maxX += delta*0.05;
        delta = maxY - minY;
        minY -= delta*0.05;
        maxY += delta*0.05;
    }
    // calc extra padding for the plots
       plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());
    plot->yAxis2->setVisible(false);
    plot->xAxis->setRangeReversed(false);
    plot->yAxis->setRangeReversed(false);

    // set axes ranges, so we see all data:
    if(graphs.size() > 0){
        plot->xAxis->setRange(minX, maxX);
        plot->yAxis->setRange(minY, maxY);
    }

    QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
      plot->xAxis->setTicker(dateTimeTicker);
    dateTimeTicker->setDateTimeFormat("d. MMM\nyyyy");
    if(quantity == TIME){
          QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
          plot->yAxis->setTicker(timeTicker);
          timeTicker->setTimeFormat("%m:%s");
      }else{
        QSharedPointer<QCPAxisTicker> fixedTicker(new QCPAxisTicker);
          plot->yAxis->setTicker(fixedTicker);
    }
    plot->legend->setWrap(3);
    plot->legend->setRowSpacing(1);
    plot->legend->setColumnSpacing(2);
    plot->legend->setFillOrder(QCPLayoutGrid::FillOrder::foColumnsFirst,true);

    plot->legend->insertColumn(1);
    plot->setInteraction(QCP::iRangeDrag, true);
    plot->setInteraction(QCP::iRangeZoom, true);

}






