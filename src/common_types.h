
#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H

#include <cstring>
#include <iostream>

#define UNUSED(expr) do { (void)(expr); } while (0)

// this dirty thing is to be able to pass
// by value an array to a function (in old C). 
// not sure I will use it at the end... possibly not
typedef struct {
    double v[6];

} sixval_t;

static inline std::ostream &operator<<(std::ostream &os, sixval_t const &m) { 
    return os << "{"<< m.v[0] << ", " << m.v[1] << ", "  << m.v[2] << ", "  << m.v[3] << ", "  << m.v[4] << ", "  << m.v[5] <<"}" ;
}

typedef enum {
    EVER = -1,
	DAYS, WEEKS,MONTHS, YEARS
} period_type_t;

typedef enum {
	NONE = -1,
	DISTANCE,
	TIME,
	STROKES,
	CALORIES,
	RESCALED_TIME,
	RESCALED_DISTANCE,
	RESTVOL
} volume_type_t ;

typedef enum {
  ALL= -1,
  FREESTYLE,
  BUTTERFLY,
  BREASTSTROKE,
  BACKSTROKE,
  DRILL,
  IM,
  MIXED,
  REST,
  STROKE_TYPE_LAST
} stroke_type_t;

static inline stroke_type_t get_stroke_type(char*tok){

	if (strncasecmp(tok,"rest",4)==0){
		return REST;
	}
	if (strncasecmp(tok,"free",4)==0){
		return FREESTYLE;
	}
	
	if (strncasecmp(tok,"butterfly",4)==0){
		return BUTTERFLY;
	}
	
	if (strncasecmp(tok,"back",4)==0){
		return BACKSTROKE;
	}
	
	if (strncasecmp(tok,"drill",4)==0){
		return DRILL;
	}
	
	if (strncasecmp(tok,"breast",4)==0){
		return BREASTSTROKE;
	}
	
	if (strncasecmp(tok,"mixed",4)==0){
		return MIXED;
	}
	
	if (strncasecmp(tok,"im",2)==0){
		return IM;
	}
	
	return MIXED;	

}

#endif


