#include <algorithm>
#include <ctime>
#include <cmath>
#include <vector>

#include "activity.h"
#include "common_types.h"
#include "swimvolumes.h"


SwimVolumes::SwimVolumes ( double pool_len ,period_type_t p, int interval_len,  stroke_type_t style, std::vector<volume_type_t> & quantities ){

	if(quantities.size() < 1) return;

	x.clear();
	y.clear();
	std::vector<Activity> activities = Activity::getActivities();
	
	x.reserve(activities.size());
	y.reserve(quantities.size());

	struct tm timeinfo;
	memset(&timeinfo,0,sizeof(timeinfo));
    timeinfo.tm_hour = 0;
	timeinfo.tm_min = 30;
    timeinfo.tm_year = activities[0].year - 1900;
    timeinfo.tm_mon = activities[0].month - 1;
    timeinfo.tm_mday = activities[0].day;

    time_t time_origin = mktime ( &timeinfo );
	const double seconds_per_day = 60.*60.*24.;


	for(auto & q : quantities){
		UNUSED(q);
		std::vector<double> z;
		z.reserve(activities.size());
		y.push_back(z);
	}
		
	long old_x = -999999999;
	for(auto & act : activities){
		if( pool_len > 0. && fabs(act.pool_len - pool_len) > 0.1 ) continue;
		// -1 = ALL. I do not use ALL below, I use interval 
		double val = act.getValue(style, interval_len ,quantities[0]);
		if ( ! (val > 0.0) ) continue ;
		
		long days = act.days_from_origin; 
		switch (p)
		{
			case WEEKS:
			days -= act.day_of_week;
			break;
			case MONTHS:
			days -= act.day ;
			break;
			case YEARS:
			days -= act.day_of_year;
			case DAYS:
			default: break;
		}
		if( days != old_x ){
			x.push_back((double)time_origin + seconds_per_day * days);
			for (unsigned long i = 0; i < quantities.size(); ++i)
				y[i].push_back(0.0);
			old_x = days;
		}

		for (unsigned long i = 0; i < quantities.size(); ++i){
			val = act.getValue(style,interval_len,quantities[i]);
			y[i].back() += val;	
		}

	}
}



SwimVolumes::SwimVolumes ( double pool_len ,period_type_t p, int interval_len,  stroke_type_t style ){

	std::vector<int> allowed_len = {25,50,100,200,400,800,1500};
	if (! std::binary_search(allowed_len.begin(), allowed_len.end(), interval_len)){	
		return;
	}

	x.clear();
	y.clear();
	std::vector<Activity> activities = Activity::getActivities();
	
	x.reserve(activities.size());
	y.reserve(1);

	struct tm timeinfo;
	memset(&timeinfo,0,sizeof(timeinfo));
    timeinfo.tm_hour = 0;
	timeinfo.tm_min = 30;
    timeinfo.tm_year = activities[0].year - 1900;
    timeinfo.tm_mon = activities[0].month - 1;
    timeinfo.tm_mday = activities[0].day;

    time_t time_origin = mktime ( &timeinfo );
	const double seconds_per_day = 60.*60.*24.;

	std::vector<double> z;
	z.reserve(activities.size());
	y.push_back(z);
		
	long old_x = -999999999;
	double cur = 99996.;
	double old_cur = cur;
	for(auto & act : activities){

		if( pool_len > 0. && fabs(act.pool_len - pool_len) > 0.1 ) continue;
		// -1 = ALL. I do not use ALL below, I use interval 
		double val = act.getBest(style, interval_len);
		if ( val > 99995. ) continue ;
		
		long days = act.days_from_origin; 
		switch (p)
		{
			case WEEKS:
			days -= act.day_of_week;
			break;
			case MONTHS:
			days -= act.day ;
			break;
			case YEARS:
			days -= act.day_of_year;
			case DAYS:
			default: break;
		}
		if (val < cur) cur = val ;

		if( days != old_x ){
			if(cur < 99996.){
				if(p>=0 || (p == EVER && cur < old_cur)){
					x.push_back((double)time_origin + seconds_per_day * days);
					y[0].push_back(cur);

					old_cur = cur;
				}
			}
			if(p>=0)
			cur = 99996.;
			old_x = days;
		}
		

	}
}




