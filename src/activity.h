

#ifndef _ACTIVITY_H_
#define _ACTIVITY_H_

#include<vector>
#include<set>
#include<string>
#include "swimlen.h"

typedef enum {
	meters,
	yards
} len_units_t;

//class Activity

class Activity {
public:
	Activity(){};
	//char file[255];
	unsigned int year;
	unsigned int month;
	unsigned int day;
	unsigned int day_of_week; // monday = 0
	unsigned int day_of_year; // Jan 1st = 0
	unsigned int days_from_origin;
	std::string filename;	
	std::string date;	
	double tot_distance;
	double tot_seconds;
	double tot_strokes;
	double tot_kcal;
	double avg_pace;
	double pool_len;
	len_units_t len_units;
	sixval_t pace;
	sixval_t seconds;
	sixval_t rescaled_seconds;
	sixval_t rescaled_distance; // I don't want mixed
	sixval_t strokes;
	sixval_t strokes_len;
	sixval_t strokes_min;
	sixval_t distance;
 	sixval_t calories;
 	double  rest;

	

 	sixval_t best25;
 	sixval_t best50;
 	sixval_t best100;
 	sixval_t best200;
 	sixval_t best400;
 	sixval_t best800;
 	sixval_t best1500;

 	std::vector<SwimInterval> intervals;

	void makeTotals();
	double getValue(stroke_type_t style,int,volume_type_t vq) const;
	double getBest(stroke_type_t style,int) const;
    static std::vector<Activity> & getActivities();
    static std::set<double> & getPools();
    static void reloadActivities(const char*);
	static void recalcDaysFromBegin();

	bool operator < (const Activity& act) const
	{
		return days_from_origin < act.days_from_origin;
	}

	static bool slowDateComparison (const Activity& act1, const Activity& act2) 
    {
		if( act1.year > act2.year || act1.year < act2.year ) return act1.year < act2.year; 
		// same year
		if(act1.month < act2.month || act1.month > act2.month) return act1.month < act2.month;
		// same month
		return ( act1.day < act2.day); 
    }

} ;


#endif







