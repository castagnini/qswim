
#ifndef SWIM_VOLUMES_H
#define SWIM_VOLUMES_H


#include <vector>

#include "activity.h"
#include "common_types.h"

class SwimVolumes {
	public:
	SwimVolumes ( double ,period_type_t , int,  stroke_type_t , std::vector<volume_type_t>&);
	SwimVolumes ( double pool_len ,period_type_t p, int interval_len,  stroke_type_t style );
	
	std::vector<double> x;
	std::vector<std::vector<double>> y;
};

#endif 

