
#include <cstring>                                
#include <dirent.h>                                
#include <errno.h>                                 
#include <sys/types.h>                             
                                                   
#include <iostream>                                
#include <time.h>                                  
#include <cmath>                                  
#include <cstring>                                  
#include <string>                                  
#include <algorithm>                                  
#include <vector>                                  


#include "preferences.h"                              
#include "activity.h"                              
#include "helpers.h"                          
#include "parse_csv_v1.h"                          
                                                   
static inline bool is_format_v1(char*name){                      
    int len = strlen(name) ;                       
    if (len < 5 ) return false;                    
    name += (len - 4);                             
    return (strcmp(name, ".txt") == 0);            
}                                                  
                                                   

static std::set <double> pools;
std::set<double> & Activity::getPools(){
	return pools;
}


static std::vector <Activity> activities;
 
std::vector<Activity> & Activity::getActivities(){
	return activities;
}

void Activity::reloadActivities(const char*path)
{
	char file[512];
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir (path)) != NULL) {
		activities.clear(); 
 		/* print all the files and directories within directory */
  		while ((ent = readdir (dir)) != NULL) {
			if( ent->d_type != DT_DIR && 
				is_format_v1(ent->d_name)) {
				strncpy(file, path,512);
				strcat(file, "/");
				strcat(file,ent->d_name);
				Activity act;
				std::string log = parse_csv_v1(file,act);
				if (log.size()) {
					std::cout<< ent->d_name << std::endl;
					std::cout<<log <<std::endl;
					std::cout << "act.date " << act.date << " int " << act.intervals.size() << std::endl;
				}else{
					activities.push_back(act);
				}
			}
  		}
  		closedir (dir);
	} else {
  		/* could not open directory */
  		std::cout<< "could not open directory" << std::endl;
	}
	
	// sort them
	std::sort(activities.begin(), activities.end(), Activity::slowDateComparison );

	// calc days from begin
	Activity::recalcDaysFromBegin();

	pools.clear();
	for(auto & act : activities) pools.insert(act.pool_len);

}


void Activity::recalcDaysFromBegin(){
	const int seconds_per_day = 60*60*24;

  	struct tm timeinfo;

  	/* get current timeinfo and modify it to the user's choice */
  	memset(&timeinfo,0,sizeof(timeinfo));
	timeinfo.tm_hour = 10;
  	timeinfo.tm_year = activities[0].year - 1900;
  	timeinfo.tm_mon = activities[0].month - 1;
  	timeinfo.tm_mday = activities[0].day;

    time_t time_origin = mktime ( &timeinfo );

	for(auto & act : activities ){
  		timeinfo.tm_year = act.year - 1900;
  		timeinfo.tm_mon = act.month - 1;
  		timeinfo.tm_mday = act.day;

    	time_t time_act = mktime ( &timeinfo );
		act.day_of_week = (timeinfo.tm_wday  -1 + 7 )% 7; 
		act.day_of_year = timeinfo.tm_yday; 
		
		double difference = difftime(time_act, time_origin) / seconds_per_day;
		if (difference < 0){
			std::cout << "problem with time ordering of activities" <<std::endl;
		}
		act.days_from_origin = (unsigned int)fabs(difference);
	}



}



#define get_single_val(activity_val, interval_val, _style) do{ \
					if (interval_len < 0) \
    	                tot += activity_val.v[_style]; \
					else \
						for(auto & slen : intervals) \
							if ( slen.style == _style && fabs(slen.distance - interval_len) < 0.5) \
								tot += slen.interval_val; \
					}while(0);

#define get_single_case(quantity, activity_val, interval_val) case quantity: \
							get_single_val(activity_val, interval_val, style) \
					break ; 

#define get_all_case(quantity, activity_val, interval_val) case quantity: \
						for (int i = 0; i< MIXED; i++) { \
							get_single_val(activity_val, interval_val, i) \
						} 	\
					break ; 

double Activity::getValue(stroke_type_t style, int interval_len, volume_type_t vq) const
{
    double tot = 0. ;
    switch (style) {
        case FREESTYLE:
        case BUTTERFLY:
        case BREASTSTROKE:
        case BACKSTROKE:
        case DRILL:
        case IM:
            switch (vq) {
				get_single_case(CALORIES,calories,kcal);
				get_single_case(DISTANCE,distance,distance);
				get_single_case(RESCALED_DISTANCE,rescaled_distance,distance);
				get_single_case(STROKES,strokes,strokes);
				get_single_case(TIME,seconds,time);
				get_single_case(RESCALED_TIME,rescaled_seconds,rescaled_time);
				
				default:
					return -99999.0;
            }
		break;
        case ALL:
            switch (vq) {
				get_all_case(CALORIES,calories,kcal);
				get_all_case(DISTANCE,distance,distance);
				get_all_case(RESCALED_DISTANCE,rescaled_distance,distance);
				get_all_case(STROKES,strokes,strokes);
				get_all_case(TIME,seconds,time);
				get_all_case(RESCALED_TIME,rescaled_seconds,rescaled_time);
				case RESTVOL:
				return rest;	
				break;
				default:
					return -99999.;
            }
		break;
        default:
            return -99999.;
    }
	return tot;
	
}

double Activity::getBest(stroke_type_t style, int interval_len) const
{
	const sixval_t * best ;
	switch(interval_len){
		case 25: best = &this->best25; break;
		case 50: best = &this->best50; break;
		case 100: best = &this->best100; break;
		case 200: best = &this->best200; break;
		case 400: best = &this->best400; break;
		case 800: best = &this->best800; break;
		case 1500: best = &this->best1500; break;
		default: return 999999.;
	}
	
    double tot = 0. ;
    switch (style) {
        case FREESTYLE:
        case BUTTERFLY:
        case BREASTSTROKE:
        case BACKSTROKE:
        case DRILL:
        case IM:
			tot = best->v[style];
            
		break;
        case ALL:
			tot = 999999.;
			for(int i =0; i<6; i++)
				if(best->v[i]<tot) tot = best->v[i];
		break;
		case MIXED:
		case REST:
		case STROKE_TYPE_LAST:
        default:
            return 99999.;
    }
	return tot;
	
}


void Activity::makeTotals(){

    rest = 0.;

    sixval_t _pace = {{0.,0.,0.,0.,0.,0.}};
    sixval_t _seconds ={{0.,0.,0.,0.,0.,0.}};
    sixval_t _rescaled_seconds ={{0.,0.,0.,0.,0.,0.}};
    sixval_t _strokes = {{0.,0.,0.,0.,0.,0.}};
    sixval_t _strokes_len = {{0.,0.,0.,0.,0.,0.}};
    sixval_t _strokes_min ={{0.,0.,0.,0.,0.,0.}};
    sixval_t _distance = {{0.,0.,0.,0.,0.,0.}};
    sixval_t _rescaled_distance = {{0.,0.,0.,0.,0.,0.}};
    sixval_t _calories = {{0.,0.,0.,0.,0.,0.}};

    sixval_t _best25 = {{99999.,99999.,99999.,99999.,99999.,99999.}};
    sixval_t _best50 = {{99999.,99999.,99999.,99999.,99999.,99999.}};
    sixval_t _best100 = {{99999.,99999.,99999.,99999.,99999.,99999.}};
    sixval_t _best200 = {{99999.,99999.,99999.,99999.,99999.,99999.}};
    sixval_t _best400 ={{99999.,99999.,99999.,99999.,99999.,99999.}};
    sixval_t _best800 = {{99999.,99999.,99999.,99999.,99999.,99999.}};
    sixval_t _best1500 = {{99999.,99999.,99999.,99999.,99999.,99999.}};

	for (auto &swimint : intervals){
        // check if it is rest
        if (swimint.is_rest) {
            rest += swimint.time;
            continue;
        }

        int style = swimint.style ;
		
        bool treatCurrentAsMixed = false ;
        if(style == IM && !Preferences::treatIMAsStyle())
            treatCurrentAsMixed = true;
        if (style < MIXED && !treatCurrentAsMixed) {

            int line_distance  = (int) swimint.distance;
            double line_seconds = swimint.time;

            _distance.v[style] += swimint.distance;
            _rescaled_distance.v[style] += swimint.distance;
            _rescaled_seconds.v[style] += rescale_time(swimint.time, swimint.distance);
            _seconds.v[style] += swimint.time;
            _calories.v[style] += swimint.kcal ;
            _strokes.v[style] +=  swimint.strokes;

            // setting bests
            if (line_distance == 25 && line_seconds < _best25.v[style] )
                _best25.v[style]  = line_seconds;
            if (line_distance == 50 && line_seconds < _best50.v[style] )
                _best50.v[style]  = line_seconds;
            if (line_distance == 100 && line_seconds < _best100.v[style] )
                _best100.v[style]  = line_seconds;
            if (line_distance== 200 && line_seconds < _best200.v[style] )
                _best200.v[style]  = line_seconds;
            if (line_distance == 400 && line_seconds < _best400.v[style] )
    			_best400.v[style]  = line_seconds;
            if (line_distance == 800 && line_seconds < _best800.v[style] )
                _best800.v[style]  = line_seconds;
            if (line_distance == 1500 && line_seconds < _best1500.v[style] )
                _best1500.v[style]  = line_seconds;

            continue;
        }

        // it's Mixed or IM to be treated as Mixed
        if (Preferences::includeMixedInAnalysis()) {
			for (auto &sub : swimint.sublengths){
                style = sub.style;
                distance.v[style] += sub.distance;
                seconds.v[style] += sub.time;
                calories.v[style] += sub.kcal ;
                strokes.v[style] +=  sub.strokes;

            }
        }
  	} // auto swimint: intervals

    for (int i = 0; i < 6; i++) {
      //  if (i == 4) continue; //skip drills
        if (i == 5 && !Preferences::treatIMAsStyle()) break;

        if (_distance.v[i] > 0 ){
            _pace.v[i] = _seconds.v[i]*100./_distance.v[i] ;
            _strokes_len.v[i] = _strokes.v[i] *50/_distance.v[i] ;
            _strokes_min.v[i] = _strokes.v[i]*60/_seconds.v[i];
        }else{
            _pace.v[i] = -1. ;
            _strokes_len.v[i] = -1 ;
            _strokes_min.v[i] = -1;
        }
    }
    strokes_len = _strokes_len;
    strokes_min = _strokes_min;
    pace = _pace;
    seconds = _seconds ;
    strokes = _strokes ;
    distance = _distance;
    rescaled_distance = _rescaled_distance;
    rescaled_seconds = _rescaled_seconds;

    calories = _calories ;

    best25 = _best25;
    best50 = _best50;
    best100 = _best100;
    best200 = _best200;
    best400 = _best400;
    best800 = _best800;
    best1500 = _best1500;

}


