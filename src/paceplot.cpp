
#include "testinput.h"
#include "paceplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"

const std::string PaceGraph::graphLabel(){
	return label;
}

const std::string PacePlot::plotTitle(){
	return std::string("Pace"); 
}

const std::string PacePlot::xAxisLabel(){
	return std::string("date"); 
}

const std::string PacePlot::y2AxisLabel(){
	return std::string("distance"); 
}

const std::string PacePlot::yAxisLabel(){
	return std::string("min/100m"); 
}

void PacePlot::validatePreferences(PlotPreferences&prefs){
	prefs.allow_period = true;
	prefs.allow_strokes = true;
	prefs.allow_pool = true;
	prefs.allow_interval = true;
	prefs.allow_points = false;
	prefs.allow_depth = false;	
	preferences = prefs;     
}

PacePlot::PacePlot(){
}

void PacePlot::createPlot(QCustomPlot* plot){
	std::vector<volume_type_t> quantities = {DISTANCE,TIME};
	double pool = preferences.pool;	
	period_type_t period = preferences.period;	
	stroke_type_t stroke = preferences.stroke;	
	int interval = preferences.interval;

	SwimVolumes sv (pool,period, interval, stroke,quantities);

	std::shared_ptr<PaceGraph> pacegraph = std::make_shared<PaceGraph>();
	std::shared_ptr<PaceGraph> distancegraph = std::make_shared<PaceGraph>();
	graphs.push_back(pacegraph);
	graphs.push_back(distancegraph);

	pacegraph->label = "pace" ;
	distancegraph->label = "distance" ;
	pacegraph->x = sv.x;
	pacegraph->y.resize(pacegraph->x.size());
	for (unsigned long i = 0 ; i< pacegraph->y.size(); ++i){
		double t = sv.y[1][i]; 
		double d = sv.y[0][i]; 
		double pace = 100.*t/d;
		pacegraph->y[i] = pace;
	}
	distancegraph->y = sv.y[0];
	distancegraph->x = sv.x;
	pacegraph->calcMinMax();
	distancegraph->calcMinMax();

	auto graph = this->graphs[0];
	// QCustomPlot needs a QVector.
	QVector<double> x= QVector<double>::fromStdVector(graph->x);
	QVector<double> y= QVector<double>::fromStdVector(graph->y);
	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(0)->setData(x, y);
	plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	plot->graph(0)->setName(graph->graphLabel().c_str());

	graph = this->graphs[1];
	plot->addGraph(plot->xAxis, plot->yAxis2);
	QPen pen;
	pen.setStyle(Qt::DashLine);
	pen.setWidth(1);
	pen.setColor(QColor(180,0,0));
	plot->graph(1)->setPen(pen);

	x= QVector<double>::fromStdVector(graph->x);
	y= QVector<double>::fromStdVector(graph->y);
	plot->graph(1)->setData(x, y);
	plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	plot->graph(1)->setName(graph->graphLabel().c_str());

	plot->yAxis2->setVisible(true);

	// Creates data from test input
	double minX,minY,maxX,maxY,minY2,maxY2;
	minX = this->minX();
	minY = this->minY();
	maxX = this->maxX();
	maxY = this->maxY();
	minY2 = graph->minY;
	maxY2 = graph->maxY;

    // calc extra padding for the plots
    	plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());
    plot->yAxis2->setLabel(this->y2AxisLabel().c_str());
	plot->xAxis->setRangeReversed(false);
	plot->yAxis->setRangeReversed(false);

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);
    plot->yAxis2->setRange(minY2, maxY2);

	QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
  	plot->xAxis->setTicker(dateTimeTicker);
	dateTimeTicker->setDateTimeFormat("d. MMM\nyyyy");
  	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  	plot->yAxis->setTicker(timeTicker);
  
  	timeTicker->setTimeFormat("%m:%s");	
	
	plot->setInteraction(QCP::iRangeDrag, true);
	plot->setInteraction(QCP::iRangeZoom, true);
	
}






