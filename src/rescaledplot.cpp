
#include "testinput.h"
#include "rescaledplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"

const std::string RescaledGraph::graphLabel(){
	return label;
}

const std::string RescaledPlot::plotTitle(){
	return std::string("Rescaled"); 
}

const std::string RescaledPlot::xAxisLabel(){
	return std::string("date"); 
}


const std::string RescaledPlot::yAxisLabel(){
	return std::string("min/100m"); 
}

void RescaledPlot::validatePreferences(PlotPreferences&prefs){
	prefs.allow_period = true;
	prefs.allow_strokes = true;
	prefs.allow_pool = true;
	prefs.allow_interval = false;
	prefs.allow_points = false;
	prefs.allow_depth = false;	
	preferences = prefs;     
}

RescaledPlot::RescaledPlot(){
}

void RescaledPlot::createPlot(QCustomPlot* plot){
	std::vector<volume_type_t> quantities = {RESCALED_DISTANCE,RESCALED_TIME};
	double pool = preferences.pool;	
	period_type_t period = preferences.period;	
	stroke_type_t stroke = preferences.stroke;	
	int interval = -1;

	SwimVolumes sv (pool,period, interval, stroke,quantities);

	std::shared_ptr<RescaledGraph> rescaledgraph = std::make_shared<RescaledGraph>();
	graphs.push_back(rescaledgraph);

	rescaledgraph->label = "rescaled" ;
	rescaledgraph->x = sv.x;
	rescaledgraph->y.resize(rescaledgraph->x.size());
	for (unsigned long i = 0 ; i< rescaledgraph->y.size(); ++i){
		double t = sv.y[1][i]; 
		double d = sv.y[0][i];
		double pace = t*100/d;
		rescaledgraph->y[i] = pace;
	}
	rescaledgraph->calcMinMax();

	auto graph = this->graphs[0];
	// QCustomPlot needs a QVector.
	QVector<double> x= QVector<double>::fromStdVector(graph->x);
	QVector<double> y= QVector<double>::fromStdVector(graph->y);
	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(0)->setData(x, y);
	plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	plot->graph(0)->setName(graph->graphLabel().c_str());


	plot->yAxis2->setVisible(false);

	// Creates data from test input
	double minX,minY,maxX,maxY;
	minX = this->minX();
	minY = this->minY();
	maxX = this->maxX();
	maxY = this->maxY();

    // calc extra padding for the plots
    	plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());
	plot->xAxis->setRangeReversed(false);
	plot->yAxis->setRangeReversed(false);

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);

	QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
  	plot->xAxis->setTicker(dateTimeTicker);
	dateTimeTicker->setDateTimeFormat("d. MMM\nyyyy");
  	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  	plot->yAxis->setTicker(timeTicker);
  
  	timeTicker->setTimeFormat("%m:%s");	
	
	plot->setInteraction(QCP::iRangeDrag, true);
	plot->setInteraction(QCP::iRangeZoom, true);
	
}






