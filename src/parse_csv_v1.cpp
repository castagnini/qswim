
#include <sstream>
#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<string>
#include<vector>
#include "activity.h"
#include "parse_csv_v1.h"

char* tokenize_line(char*line, char*tokens, int tok_size, int maxlen, int *found){

	*found = 0 ;
	if(*line == '\0') return NULL;

	char*tok_ptr = tokens ; // + *found * maxlen; // found is 0 here
	char*ptr = line;
	int esc =0;

	memset(tokens,0,tok_size);

	while(*ptr == ' '|| *ptr == '\t') ptr++; //remove trailing spaces
    
	while(1){
        if(*ptr == '"'){
            esc = !esc ;
			ptr++;
			continue;
        }
        else if(*ptr == '\n' || *ptr == '\0'){
			// end of line = end of while.
			// token found if line was not empty.
			*tok_ptr = '\0';
			(*found)++;
			ptr++;
			break;
        }else if(esc && *ptr == ','){
			ptr++; // removes commas in "1,000"
            continue;
        }else if(!esc && *ptr == ','){
			// token found if line was not empty.
			*tok_ptr = '\0';
			(*found)++;
			tok_ptr = tokens + (*found) * maxlen;
			ptr++;
			while(*ptr == ' '|| *ptr == '\t') ptr++; //remove trailing spaces
			continue;
		}else{
        	*tok_ptr = *ptr ;
			tok_ptr++;
       		ptr++;
		}
    }
	
	return ptr;

}

char*read_file(char*filename, file_info_p info){
        FILE * fp;
        if( (fp=fopen(filename,"r") ) == 0){
                fprintf(stderr,"Could not open file: %s\n",filename);
                exit(1);
        }
		
		off_t filesize = get_filesize(fp);
		// put a check on filesize < 1Mb;
		char*buf = (char*)malloc((size_t)filesize+1);	
		fread(buf, 1, filesize, fp);
		fclose(fp);
		buf[filesize] = '\0';

        unsigned long n_lines = 0, c=0, s=0, line_size_max=0,tokens_max=0;
        int i,esc=0;
        char*ptr;
        // count lines
        ptr = buf;
		i = (size_t)filesize;
        while(i--){
			c++;
			if(*ptr == '"'){
				esc = !esc ;
			}
        	else if(*ptr == '\n'){
				if(esc){
					 fprintf (stderr,"Error: escaped sequence not closed within a line. I stop here\n");
					exit(1);
				}
           		n_lines++;
				if(c>line_size_max) line_size_max = c;
				if(s>tokens_max) tokens_max = s;
				c=0;
				s=0;
			}else if(!esc && *ptr == ','){
				s++;	
			}
        	++ptr;
			
        }
		if(c>0 && c>line_size_max) line_size_max = c;
		if(s>0 && s>tokens_max) tokens_max = s;
	//	if(esc) { fprintf (stderr,"Error: escaped sequence not closed.\n");}
	//	printf("n_lines:%lu\nmax charactersper line:%lu\nmax tokens per line:%lu\n",n_lines,line_size_max,tokens_max);

		info->filesize = (unsigned long)filesize;
		info->n_lines = n_lines;
		info->line_size = line_size_max+1;
		info->tokens = tokens_max+1;
        return buf;
}

static inline bool check_header(int tot,const char*h[],char*tok,int line_size){
	int i;
	for (i = 0 ; i<tot; i++){
		if(strncmp(h[i],&tok[i*line_size],strlen(h[i])) !=0) {
			printf("%s %s \n",h[i],&tok[i*line_size]);
			return false;
		}

	}
	return true;
}

void parse_session_fields(Activity &a, char*tok,int line_size){
	a.date = tok;
	char*p = strtok(tok,"-");
	a.year = atoi(p);
	p = strtok(NULL, "-");
	a.month = atoi(p);
	p = strtok(NULL, "-");
	a.day = atoi(p);
	a.tot_distance = atof( &tok[1*line_size] );
	if(tok[2*line_size] == 'm') a.len_units = meters;
	a.tot_seconds = atof( &tok[3*line_size] );
	a.avg_pace = a.tot_seconds*100.0/a.tot_distance;
	a.tot_strokes = atof( &tok[5*line_size] );
	a.tot_kcal = atof( &tok[6*line_size] );
	a.pool_len =  atof( &tok[7*line_size] );
}

std::string parse_csv_v1(char*filename, Activity &act){
	std::ostringstream log;
	file_info_t info;
	char*buf = read_file(filename, &info);
	long token_buf_size = info.line_size*info.tokens;
	char*tokens = (char*)malloc(token_buf_size);
	int tok_found=0;
	
	const char *h_session[] = {"start_time","total_distance","total_distance_units",
           "total_timer_time","total_timer_time_units","total_cycles",
           "total_calories","pool_length","pool_length_units"};
	int h_session_num = sizeof(h_session)/sizeof(char*);
	const char *h_fields[] = {"split","swim_stroke","total_distance","total_distance_units",
                    "total_timer_time","total_timer_time_units",
                    "total_cycles","total_calories"};
	int h_fields_num = sizeof(h_fields)/sizeof(char*);

	char* nextline = tokenize_line(buf,tokens,token_buf_size,info.line_size,&tok_found);	
	if(strncmp("Version:1.0",tokens,11) != 0) {
		log << "wrong version\n";
		goto dealloc;
	}
	
	act.filename = filename;
		
	// session header
	nextline = tokenize_line(nextline,tokens,token_buf_size,info.line_size,&tok_found);
	if(!check_header(h_session_num,h_session,tokens,info.line_size))
	{
		log  << "wrong session header\n";
		goto dealloc;
	}

	// session values
	nextline = tokenize_line(nextline,tokens,token_buf_size,info.line_size,&tok_found);
	parse_session_fields(act, tokens,info.line_size);
	
	// fields header
	nextline = tokenize_line(nextline,tokens,token_buf_size,info.line_size,&tok_found);
	if(!check_header(h_fields_num,h_fields,tokens,info.line_size))
	{
		log << "wrong fields header\n";
		goto dealloc;
	}

	
	while((nextline = tokenize_line(nextline,tokens,token_buf_size,info.line_size,&tok_found)) != NULL)	
	{
		if(strncasecmp(tokens,"lap",3)==0){
			stroke_type_t sty=get_stroke_type(&tokens[1*info.line_size]);
			double m = atof(&tokens[2*info.line_size]);
			double t = atof(&tokens[4*info.line_size]);
			double strk = atof(&tokens[6*info.line_size]); 
			double c = atof(&tokens[7*info.line_size]);
			int num_sublen = (int)(m/act.pool_len);
  			//SwimInterval lap(sty,m,t,strk,c,num_sublen);
			//act.intervals.push_back(lap);
			act.intervals.emplace_back(sty,m,t,strk,c,num_sublen);
		}else if (strncasecmp(tokens,"rest",4)==0){
			stroke_type_t sty=REST;
			double m = 0.;
			double t = atof(&tokens[4*info.line_size]);
			double strk = 0.; 
			double c = 0.;
			int num_sublen = 0.;
  			//SwimInterval lap(sty,m,t,strk,c,num_sublen);
			//act.intervals.push_back(lap);
			act.intervals.emplace_back(sty,m,t,strk,c,num_sublen);
		}else{
			stroke_type_t sty=get_stroke_type(&tokens[1*info.line_size]);
			double m = atof(&tokens[2*info.line_size]);
			double t = atof(&tokens[4*info.line_size]);
			double strk = atof(&tokens[6*info.line_size]); 
			double c = atof(&tokens[7*info.line_size]);
  			//SwimLen swimlen(sty,m,t,strk,c);
			SwimInterval &lap = act.intervals.back();
			lap.sublengths.emplace_back(sty,m,t,strk,c);
		}
	}

	for (auto &swimint: act.intervals) swimint.setIsIM();
	act.makeTotals();


dealloc:
	return log.str();
}









