
#include "testinput.h"
#include "strokesplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"

const std::string StrokeGraph::graphLabel(){
	return label;
}

const std::string StrokePlot::plotTitle(){
	return std::string("Stroke"); 
}

const std::string StrokePlot::xAxisLabel(){
	return std::string("date"); 
}

const std::string StrokePlot::y2AxisLabel(){
	return std::string("distance"); 
}

const std::string StrokePlot::yAxisLabel(){
	return std::string("min/100m"); 
}

void StrokePlot::validatePreferences(PlotPreferences&prefs){
	prefs.allow_period = true;
	prefs.allow_strokes = true;
	prefs.allow_pool = true;
	prefs.allow_interval = true;
	prefs.allow_points = false;
	prefs.allow_depth = false;	
	preferences = prefs;     
}

StrokePlot::StrokePlot(){
}

void StrokePlot::createPlot(QCustomPlot* plot){
	std::vector<volume_type_t> quantities = {DISTANCE,TIME,STROKES};
	double pool = preferences.pool;	
	period_type_t period = preferences.period;	
	stroke_type_t stroke = preferences.stroke;	
	int interval = preferences.interval;

	std::cout << pool << " "<<  period << " "<<  interval << " "<< stroke << std::endl;	
	
	SwimVolumes sv (pool,period, interval, stroke,quantities);

	std::shared_ptr<StrokeGraph> stroke_len_graph = std::make_shared<StrokeGraph>();
	std::shared_ptr<StrokeGraph> stroke_min_graph = std::make_shared<StrokeGraph>();
	graphs.push_back(stroke_len_graph);
	graphs.push_back(stroke_min_graph);

	stroke_len_graph->label = "stroke per len" ;
	stroke_min_graph->label = "stroke per min" ;
	stroke_len_graph->x = sv.x;
	stroke_len_graph->y.resize(stroke_len_graph->x.size());
	stroke_min_graph->x = sv.x;
	stroke_min_graph->y.resize(stroke_min_graph->x.size());
	for (int i = 0 ; i< stroke_len_graph->y.size(); ++i){
		double strk = sv.y[2][i]; 
		double t = sv.y[1][i]; 
		double d = sv.y[0][i]; 
		double stroke_per_min = strk*60./t ;
		double stroke_per_len = strk*50./d ;
		stroke_len_graph->y[i] = stroke_per_len;
		stroke_min_graph->y[i] = stroke_per_min;
	}
	stroke_len_graph->calcMinMax();
	stroke_min_graph->calcMinMax();
	std::cout << __func__ << std::endl;

	auto graph = this->graphs[0];
	// QCustomPlot needs a QVector.
	QVector<double> x= QVector<double>::fromStdVector(graph->x);
	QVector<double> y= QVector<double>::fromStdVector(graph->y);
	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(0)->setData(x, y);
	plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	plot->graph(0)->setName(graph->graphLabel().c_str());

	graph = this->graphs[1];
	plot->addGraph(plot->xAxis, plot->yAxis2);
	QPen pen;
	pen.setStyle(Qt::DashLine);
	pen.setWidth(1);
	pen.setColor(QColor(180,0,0));
	plot->graph(1)->setPen(pen);

	x= QVector<double>::fromStdVector(graph->x);
	y= QVector<double>::fromStdVector(graph->y);
	plot->graph(1)->setData(x, y);
	plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	plot->graph(1)->setName(graph->graphLabel().c_str());

	plot->yAxis2->setVisible(true);

	// Creates data from test input
	double minX,minY,maxX,maxY,minY2,maxY2;
	minX = this->minX();
	minY = this->minY();
	maxX = this->maxX();
	maxY = this->maxY();
	minY2 = graph->minY;
	maxY2 = graph->maxY;

    // calc extra padding for the plots
    	plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());
    plot->yAxis2->setLabel(this->y2AxisLabel().c_str());

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);
    plot->yAxis2->setRange(minY2, maxY2);
	plot->xAxis->setRangeReversed(false);
	plot->yAxis->setRangeReversed(false);

	QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
  	plot->xAxis->setTicker(dateTimeTicker);
	dateTimeTicker->setDateTimeFormat("d. MMM\nyyyy");
//  	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
//  	plot->yAxis->setTicker(timeTicker);
  
 // 	timeTicker->setTimeFormat("%m:%s");	
	
	plot->setInteraction(QCP::iRangeDrag, true);
	plot->setInteraction(QCP::iRangeZoom, true);
	
}






