
#include "testinput.h"
#include "restplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"

const std::string RestGraph::graphLabel(){
    return label;
}

const std::string RestPlot::plotTitle(){
    return std::string("Rest");
}

const std::string RestPlot::xAxisLabel(){
    return std::string("date");
}

const std::string RestPlot::y2AxisLabel(){
    return std::string("%");
}

const std::string RestPlot::yAxisLabel(){
    return std::string("time");
}

void RestPlot::validatePreferences(PlotPreferences&prefs){
    prefs.allow_period = true;
    prefs.allow_strokes = false;
    prefs.allow_pool = true;
    prefs.allow_interval = false;
    prefs.allow_points = false;
    prefs.allow_depth = false;
    preferences = prefs;
}

RestPlot::RestPlot(){
}

void RestPlot::createPlot(QCustomPlot* plot){
    std::vector<volume_type_t> quantities = {TIME,RESTVOL};
    double pool = preferences.pool;
    period_type_t period = preferences.period;
    stroke_type_t stroke = ALL;
    int interval = -1;

    SwimVolumes sv (pool,period, interval, stroke,quantities);

    std::shared_ptr<RestGraph> restgraph = std::make_shared<RestGraph>();
    std::shared_ptr<RestGraph> per100graph = std::make_shared<RestGraph>();
    graphs.push_back(restgraph);
    graphs.push_back(per100graph);

    restgraph->label = "rest" ;
    per100graph->label = "%" ;
    restgraph->x = sv.x;
    per100graph->x = sv.x;
    restgraph->y.resize(restgraph->x.size());
    per100graph->y.resize(per100graph->x.size());
    for (unsigned long i = 0 ; i< restgraph->x.size(); ++i){
        double t = sv.y[0][i];
        double r = sv.y[1][i];
        double rest100 = r*100. / (t+r);
        restgraph->y[i] = r;
        per100graph->y[i] = rest100;
    }
    restgraph->calcMinMax();
    per100graph->calcMinMax();

    auto graph = this->graphs[0];
    // QCustomPlot needs a QVector.
    QVector<double> x= QVector<double>::fromStdVector(graph->x);
    QVector<double> y= QVector<double>::fromStdVector(graph->y);
    plot->addGraph(plot->xAxis, plot->yAxis);
    plot->graph(0)->setData(x, y);
    plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

    plot->graph(0)->setName(graph->graphLabel().c_str());

    graph = this->graphs[1];
    plot->addGraph(plot->xAxis, plot->yAxis2);
    QPen pen;
    pen.setStyle(Qt::DashLine);
    pen.setWidth(1);
    pen.setColor(QColor(180,0,0));
    plot->graph(1)->setPen(pen);

    x = QVector<double>::fromStdVector(graph->x);
    y = QVector<double>::fromStdVector(graph->y);
    plot->graph(1)->setData(x, y);
    plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

    plot->graph(1)->setName(graph->graphLabel().c_str());

    plot->yAxis2->setVisible(true);

    // Creates data from test input
    double minX,minY,maxX,maxY,minY2,maxY2;
    minX = this->minX();
    minY = this->minY();
    maxX = this->maxX();
    maxY = this->maxY();
    minY2 = graph->minY;
    maxY2 = graph->maxY;

    // calc extra padding for the plots
    plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());
    plot->yAxis2->setLabel(this->y2AxisLabel().c_str());

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);
    plot->yAxis2->setRange(minY2, maxY2);
    plot->xAxis->setRangeReversed(false);
    plot->yAxis->setRangeReversed(false);

    QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
    plot->xAxis->setTicker(dateTimeTicker);
    dateTimeTicker->setDateTimeFormat("d. MMM\nyyyy");
    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    plot->yAxis->setTicker(timeTicker);
    QSharedPointer<QCPAxisTicker> normalTicker(new QCPAxisTicker);
    plot->yAxis2->setTicker(normalTicker);

    timeTicker->setTimeFormat("%m:%s");

    plot->setInteraction(QCP::iRangeDrag, true);
    plot->setInteraction(QCP::iRangeZoom, true);

}






