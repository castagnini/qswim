

#ifndef _PACEPLOT_H_
#define _PACEPLOT_H_

#include <string>
#include <vector>
#include <memory>
#include "baseplot.h"


class SwimSmoothUpperGraph : public BaseGraph {
	public:
		PaceGraph(): BaseGraph() { }
		void populate();
		const std::string graphLabel() { return ""; }
};

class PacePlot : public BasePlot {
	public:
		PacePlot() : BasePlot() { }
		const std::string plotTitle() ;
		const std::string xAxisLabel() ;
		const std::string yAxisLabel() ;

	double minX () {return ( graphs.size()>0 ? graphs[0]->minX : 0) ;}
	double maxX () {return ( graphs.size()>0 ? graphs[0]->maxX : 0) ;}
	double minY () {return ( graphs.size()>0 ? graphs[0]->minY : 0) ;}
	double maxY () {return ( graphs.size()>0 ? graphs[0]->maxY : 0) ;}

};


#endif


