

#ifndef _RESTPLOT_H_
#define _RESTPLOT_H_

#include <string>
#include <vector>
#include <memory>
#include "baseplot.h"


class RestGraph : public BaseGraph {
	public:
		RestGraph(): BaseGraph() { }
		const std::string graphLabel();

	std::string label;
};

class RestPlot : public BasePlot {
	public:
		RestPlot() ;
		const std::string plotTitle() ;
		const std::string xAxisLabel() ;
		const std::string yAxisLabel() ;

	void validatePreferences(PlotPreferences&);
	void createPlot(QCustomPlot* plot);
    const std::string y2AxisLabel();
	double minX () {return ( graphs.size()>0 ? graphs[0]->minX : 0) ;}
	double maxX () {return ( graphs.size()>0 ? graphs[0]->maxX : 0) ;}
	double minY () {return ( graphs.size()>0 ? graphs[0]->minY : 0) ;}
	double maxY () {return ( graphs.size()>0 ? graphs[0]->maxY : 0) ;}

};


#endif


