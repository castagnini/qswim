

#ifndef _SMOOTHPLOT_H_
#define _SMOOTHPLOT_H_

#include <string>
#include <vector>
#include <memory>
#include "baseplot.h"


class SmoothGraph : public BaseGraph {
	public:
		SmoothGraph(): BaseGraph() { }
		const std::string graphLabel();

	std::string label;
};

class SmoothPlot : public BasePlot {
	public:
		SmoothPlot() ;
		const std::string plotTitle() ;
		const std::string xAxisLabel() ;
		const std::string yAxisLabel() ;

	void validatePreferences(PlotPreferences&);
	void createPlot(QCustomPlot* plot);

	double minX () {return ( graphs.size()>0 ? graphs[0]->minX : 0) ;}
	double maxX () {return ( graphs.size()>0 ? graphs[0]->maxX : 0) ;}
	double minY () {return ( graphs.size()>0 ? graphs[0]->minY : 0) ;}
	double maxY () {return ( graphs.size()>0 ? graphs[0]->maxY : 0) ;}

};


#endif


