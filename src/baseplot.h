

#ifndef _BASEPLOT_H_
#define _BASEPLOT_H_

#include <iostream>
#include <tuple>
#include <string>
#include <vector>
#include <memory>
#include "testinput.h"
#include "common_types.h"
#include "qcustomplot.h"


class PlotPreferences  {
	public: 
	PlotPreferences()
	{
		allow_strokes = true;
		allow_all_strokes = false;
		stroke = FREESTYLE;

		allow_period = true;
		period = DAYS;

		allow_pool = true ;
		pool = -1.;

		allow_interval = false;
		interval = 50;

		allow_points = false;
		points = 5;

		allow_depth = false;
		depth = 3;
	}

	bool allow_strokes;
	bool allow_all_strokes;
	stroke_type_t stroke;

	bool allow_period;
	period_type_t period;

	bool allow_pool;
	double pool;

	bool allow_interval;
	int interval;
	
	bool allow_points;
	int points;

	bool allow_depth;
	int depth;
	
} ;

// A BasePlot can contain many BaseGraphs
// All Graphs are derived from BaseGraph
// Same for plots.
// BasseGraph points can be populated either in BaseGraph 
// or in BasePlot. For instance, if 2 quantities are shown 
// which share same x-axis, it doesn't make sense to compute
// them twice.

class BaseGraph {
	public:
		BaseGraph() { }
		virtual ~ BaseGraph() = default;
		virtual const std::string graphLabel() {return "";};
		virtual void calcMinMax(){
			if(x.size() <1) return;
    		minX=maxX=x[0];
    		minY=maxY=y[0];
    		for (unsigned long  i=0; i<x.size(); ++i)
    		{
      			// finds max and min for the plot ranges
      			minX = minX<x[i]?minX:x[i];
				minY = minY<y[i]?minY:y[i];
				maxX = maxX>x[i]?maxX:x[i];
				maxY = maxY>y[i]?maxY:y[i];
			}
			double delta = maxX - minX;
			minX -= delta*0.05;
			maxX += delta*0.05;
			delta = maxY - minY;
			minY -= delta*0.05;
			maxY += delta*0.05;

		}
		std::vector<double> x , y;
		double minX,minY, maxX, maxY;
};

class BasePlot {
	public:
		BasePlot() { }
		virtual ~ BasePlot() = default;
		virtual const std::string plotTitle() {return "";};
		virtual const std::string xAxisLabel() {return "";};
		virtual const std::string yAxisLabel() {return "";};
		virtual const std::string y2AxisLabel() {return "";};

		virtual void createPlot(QCustomPlot*) = 0 ;

		std::vector<std::shared_ptr<BaseGraph> > graphs ;
		virtual double minX() = 0; 
		virtual double maxX() = 0; 
		virtual double minY() = 0; 
		virtual double maxY() = 0; 
		
		virtual void validatePreferences(PlotPreferences&prefs){
			// check Prefs and makes a copy

			preferences = prefs;
		}; 

		PlotPreferences preferences;	
};


class DummyGraph : public BaseGraph{
	public:
	DummyGraph ():BaseGraph()
	{
    	long len_data = test_input_len();
		y.resize(len_data);
		x.resize(len_data);
    	minX=maxX=testX[0];
    	minY=maxY=testY[0];
    	for (int i=0; i<len_data; ++i)
    	{
      		x[i] = testX[i];
      		y[i] = testY[i];
      		// finds max and min for the plot ranges
      		minX = minX<x[i]?minX:x[i];
      		minY = minY<y[i]?minY:y[i];
      		maxX = maxX>x[i]?maxX:x[i];
      		maxY = maxY>y[i]?maxY:y[i];
    	}
	}
	const std::string graphLabel() {return "pace min/100m";}

};

class DummyPlot :public BasePlot {
	public:
	DummyPlot ():BasePlot() {
		preferences.allow_period = false;
		graphs.push_back(std::make_shared<DummyGraph>());
	}
	double minX () {return ( graphs.size()>0 ? graphs[0]->minX : 0) ;}
	double maxX () {return ( graphs.size()>0 ? graphs[0]->maxX : 0) ;}
	double minY () {return ( graphs.size()>0 ? graphs[0]->minY : 0) ;}
	double maxY () {return ( graphs.size()>0 ? graphs[0]->maxY : 0) ;}
	const std::string plotTitle() {return "Swimming Pace Data" ;}
	const std::string xAxisLabel() {return "days from begin" ; } 
	const std::string yAxisLabel() {return "swim pace for 100m (seconds)"; }


// example of creating a plot
void createPlot(QCustomPlot* plot){

	for (auto graph : this->graphs ){
    	// QCustomPlot needs a QVector.
    	QVector<double> x= QVector<double>::fromStdVector(graph->x);
		QVector<double> y= QVector<double>::fromStdVector(graph->y);
    	plot->addGraph();
    	plot->graph()->setData(x, y);
    	plot->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));
		
    	plot->graph()->setName(graph->graphLabel().c_str());
	}
   // Creates data from test input
    double minX,minY,maxX,maxY;
	minX = this->minX();
	minY = this->minY();
	maxX = this->maxX();
	maxY = this->maxY();

    // calc extra padding for the plots
    double delta = maxX - minX;
    minX -= delta*0.05;
    maxX += delta*0.05;
    delta = maxY - minY;
    minY -= delta*0.05;
    maxY += delta*0.05;
 	plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);

    plot->replot();
	
}

};


#endif


