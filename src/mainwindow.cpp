
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <tuple>
#include <cmath>
#include <QListWidget>
#include "baseplot.h"
#include "paceplot.h"
#include "bestplot.h"
#include "rescaledplot.h"
#include "bestintplot.h"
#include "strokesplot.h"
#include "smoothplot.h"
#include "restplot.h"
#include "volumeplot.h"
#include "mainwindow.h"
#include "testinput.h"
#include "common_types.h"
#include "activity.h"


std::map<std::string,stroke_type_t> stroke_map = { 
{"All", ALL}, {"Freestyle", FREESTYLE}, {"Butterfly",BUTTERFLY},
{"Breaststroke",BREASTSTROKE},{"Backstroke",BACKSTROKE},{"Drill",DRILL},{"IM",IM}
} ;

std::map<std::string,double> pool_map ;
std::map<std::string,int> interval_map ;
std::map<std::string,period_type_t> period_map = {{"Days",DAYS},{"Weeks",WEEKS},
{"Months",MONTHS},{"Years",YEARS}} ;

std::map<std::string,int> points_map = {{"All activities",-1},
{"Last activity",1},
{"Last 5 activities",5},
{"Last 10 activities",10},
{"Last 20 activities",20},
{"Last 50 activities",50},
{"Last 100 activities",100}};
std::map<std::string,int> depth_map ;

void MainWindow::refreshPreferences(){
	plotprefs.pool = pool_map[pools_combo->currentText().toStdString()];
	plotprefs.period = period_map[period_combo->currentText().toStdString()];
	plotprefs.stroke = stroke_map[styles_combo->currentText().toStdString()];
	plotprefs.interval = interval_map[interval_combo->currentText().toStdString()];
	plotprefs.points = points_map[points_combo->currentText().toStdString()];
	plotprefs.depth = pool_map[depth_combo->currentText().toStdString()];
}

std::shared_ptr<BasePlot> createDummyPlot () {  return std::make_shared<DummyPlot>();}
std::shared_ptr<BasePlot> createPacePlot () {  return std::make_shared<PacePlot>();}
std::shared_ptr<BasePlot> createRescaledPlot () {  return std::make_shared<RescaledPlot>();}
std::shared_ptr<BasePlot> createStrokePlot () { return std::make_shared<StrokePlot>();}
std::shared_ptr<BasePlot> createDistancePlot() { 
	std::shared_ptr<VolumePlot> plot =  std::make_shared<VolumePlot>();
	plot->quantity = DISTANCE;
	return plot;
}
std::shared_ptr<BasePlot> createTimePlot()  {  
	std::shared_ptr<VolumePlot> plot =  std::make_shared<VolumePlot>();
	plot->quantity = TIME;
	return plot;
}
std::shared_ptr<BasePlot> createStrokeVolumePlot() { 
	std::shared_ptr<VolumePlot> plot =  std::make_shared<VolumePlot>();
	plot->quantity = STROKES;
	return plot;
}

std::shared_ptr<BasePlot> createBestEverPlot() {  return std::make_shared<BestPlot>(true);}
std::shared_ptr<BasePlot> createBestOfPeriodPlot() {  return std::make_shared<BestPlot>();}
std::shared_ptr<BasePlot> createBestOverIntervalsPlot() {  return std::make_shared<BestIntPlot>();}
std::shared_ptr<BasePlot> createRestPlot () { return std::make_shared<RestPlot>();}
std::shared_ptr<BasePlot> createSwimSmoothPlot() {  return std::make_shared<SmoothPlot>();}


typedef enum {
	PACE_PLOT,
	STROKE_PLOT,
	DISTANCE_PLOT,
	TIME_PLOT,
	STROKE_VOL_PLOT,
	BEST_EVER_PLOT,
	BEST_OF_PERIOD_PLOT,
	BEST_OVER_INT_PLOT,
	REST_PLOT,
	SWIMSMOOTH_PLOT,
	RESCALED_PLOT,
	DISTRO_PLOT,

	LAST_UNUSED_PLOT
} plot_type_enum ;

typedef std::shared_ptr<BasePlot> (*plot_create_func)();

typedef struct {
	std::string group; 
	std::string name; 
	plot_type_enum type ;
	plot_create_func create; 
} plot_info;

std::vector<plot_info> plot_list =  {
{ "Basics", "pace", PACE_PLOT, createPacePlot},
{ "Basics", "stroke rate", STROKE_PLOT, createStrokePlot },
{ "Basics", "rescaled pace", RESCALED_PLOT, createRescaledPlot },
{"Volumes", "distance", DISTANCE_PLOT, createDistancePlot },
{"Volumes", "time", TIME_PLOT, createTimePlot },
{"Volumes", "strokes", STROKE_VOL_PLOT, createStrokeVolumePlot },
{"Records", "best ever", BEST_EVER_PLOT, createBestEverPlot},
{"Records", "best of day/week/month" , BEST_OF_PERIOD_PLOT, createBestOfPeriodPlot },
{"Records", "best over intervals" , BEST_OVER_INT_PLOT, createBestOverIntervalsPlot },
{"Extra", "rest", REST_PLOT , createRestPlot },
{"Extra", "swimsmooth", SWIMSMOOTH_PLOT, createSwimSmoothPlot  }
};

void MainWindow::itemCollapsed(QTreeWidgetItem *item){
	item->setExpanded(true);
//	std::cout<< "collapsed" << std::endl;
}

void MainWindow::currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous) 
{
	static bool do_nothing = false;
	if (do_nothing)  return;
    QVariant data = current->data(0,Qt::UserRole);
    unsigned int i = data.toUInt();
    if( i < LAST_UNUSED_PLOT && i < plot_list.size()) {
        std::cout<< plot_list[i].name << std::endl;
		this->InitPlot(i);
    }else{
		do_nothing = true;
		// the following will raise another currentItemChanged
		// but we do_nothing 
		tree->setCurrentItem(previous);
		do_nothing = false;
	}

}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    // Set size of the window
    resize(1000, 600);

	toolbox = new QToolBox(this);
	QWidget * activitypanel = new QWidget(this);
	QWidget * leftpanel = new QWidget(this);
	QVBoxLayout * activitylayout = new QVBoxLayout ();
	QVBoxLayout * leftlayout = new QVBoxLayout ();
    activitypanel->setLayout(activitylayout);
	leftpanel->setLayout(leftlayout);
	activitylayout->addWidget(new QLabel("Activities:"));	
	leftlayout->addWidget(new QLabel("Analysis:"));	
	QWidget * rightpanel = new QWidget(this);
	QVBoxLayout * rightlayout = new QVBoxLayout ();
	rightpanel->setLayout(rightlayout);
	rightlayout->addWidget(new QLabel("Preferences:"));	
	rightlayout->addWidget(toolbox);
	activitytree = new QTreeWidget();
	tree = new QTreeWidget();
	leftlayout->addWidget(tree);	
	activitylayout->addWidget(activitytree);	
    plot = new  QCustomPlot (this);

	std::string old_group = "";
	QTreeWidgetItem *treeItem = nullptr;
	for(unsigned int i = 0; i<plot_list.size() ; ++i){
		if(plot_list[i].group != old_group){
			treeItem = new QTreeWidgetItem(tree);
		
			treeItem->setTextAlignment (0,Qt::AlignHCenter);
			treeItem->setText(0, plot_list[i].group.c_str());
 			treeItem->setFlags(treeItem->flags() ^ Qt::ItemIsSelectable);
			treeItem->setFlags(Qt::ItemIsEnabled);
			treeItem->setForeground(0, Qt::white);
			treeItem->setBackground(0, Qt::gray);
			QVariant data = QVariant(uint(LAST_UNUSED_PLOT));
			treeItem->setData(0,Qt::UserRole,data);	
		}
		old_group = plot_list[i].group ;
		
		QTreeWidgetItem *childItem = new QTreeWidgetItem();
		childItem->setText(0, plot_list[i].name.c_str());
		QVariant data = QVariant(i);
		childItem->setData(0,Qt::UserRole,data);	
		treeItem->addChild(childItem);
		if(!i) tree->setCurrentItem(childItem);
		
	}

    std::vector<Activity> activities = Activity::getActivities();

	for(auto & act : activities){ 
		
		QTreeWidgetItem *childItem = new QTreeWidgetItem(activitytree);
        std::string label = act.date;
		childItem->setText(0, label.c_str());
		/*
 *      QVariant data = QVariant(i);
		childItem->setData(0,Qt::UserRole,data);	
		if(!i) tree->setCurrentItem(childItem);
		*/
	}

	connect(tree,SIGNAL(itemCollapsed(QTreeWidgetItem *)),this,SLOT(itemCollapsed(QTreeWidgetItem *)));
	connect(tree,SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)),this,SLOT(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)));
	tree->setHeaderHidden(true);
	tree->setIndentation(0);
	tree->expandAll();
	leftlayout->addWidget(tree);

	QWidget * plotprefs = new QWidget(this);
    QVBoxLayout * plotprefslayout = new QVBoxLayout ();
	plotprefs->setLayout(plotprefslayout);

	pools_label = new QLabel("Pools:");
	plotprefslayout->addWidget(pools_label);
	pools_combo = new QComboBox();
	pools_combo->addItem("All");
	for (double d : Activity::getPools()){
		std::string tmp_str = std::to_string(d);
		pool_map[tmp_str] = d;
		pools_combo->addItem(tmp_str.c_str());	
	}
	plotprefslayout->addWidget(pools_combo);
	connect(pools_combo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),
    [=](const QString &text){ UNUSED(text); InitPlot(current_plot_idx);  });

	styles_label = new QLabel("Strokes:");
	plotprefslayout->addWidget(styles_label);
	styles_combo = new QComboBox();
	for (auto & map: stroke_map) 
		styles_combo->addItem(map.first.c_str());
	plotprefslayout->addWidget(styles_combo);
	connect(styles_combo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),
    [=](const QString &text){ UNUSED(text); InitPlot(current_plot_idx);  });


	period_label = new QLabel("Period:");	
	plotprefslayout->addWidget(period_label);
	period_combo = new QComboBox();
	for (auto & map: period_map)
		period_combo->addItem(map.first.c_str());
	plotprefslayout->addWidget(period_combo);
	connect(period_combo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),
    [=](const QString &text){ UNUSED(text); InitPlot(current_plot_idx);  });
	
	interval_label = new QLabel("Interval length:");
	plotprefslayout->addWidget(interval_label);
	interval_combo = new QComboBox();
	{
		interval_map["All"] = -1;
		interval_combo->addItem("All");
		std::vector<int> tmp = {25,50,100,200,400,800,1500};
		for (auto & v : tmp){
			std::string tmp_str = std::to_string(v);
			interval_map[tmp_str] = v;
			interval_combo->addItem(tmp_str.c_str());
		}
	}
	plotprefslayout->addWidget(interval_combo);
	connect(interval_combo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),
    [=](const QString &text){ UNUSED(text); InitPlot(current_plot_idx);  });

	points_label = new QLabel("Points:");
	plotprefslayout->addWidget(points_label);
	points_combo = new QComboBox();
	for (auto & map: points_map) 
		points_combo->addItem(map.first.c_str());
	plotprefslayout->addWidget(points_combo);
	connect(points_combo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),
    [=](const QString &text){ UNUSED(text); InitPlot(current_plot_idx);  });

	depth_label = new QLabel("Depth:");
	plotprefslayout->addWidget(depth_label);
    depth_combo = new QComboBox();
    for(int i=1;i<9;i++) {
		std::string tmp_str = std::to_string(i);
		depth_map[tmp_str] = i;
		depth_combo->addItem(tmp_str.c_str());
    }	
	plotprefslayout->addWidget(depth_combo);
	connect(depth_combo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),
    [=](const QString &text){UNUSED(text); InitPlot(current_plot_idx);  });
	
    plotprefslayout->setAlignment(Qt::AlignTop);


	toolbox->addItem(plotprefs,"Current Plot");
	toolbox->addItem(new QPushButton("ciao2"),"Global");
	QSplitter *splitter = new QSplitter;

    QSplitter *leftsplitter = new QSplitter;

    leftsplitter->setOrientation(Qt::Vertical);
    
    leftsplitter->addWidget(activitypanel);
    leftsplitter->addWidget(leftpanel);

	splitter->addWidget(leftsplitter); 
	splitter->addWidget(plot); 
	splitter->addWidget(rightpanel); 

	QList<int> splitter_sizes;
	splitter_sizes << 150 << 500 <<150;
	splitter->setSizes(splitter_sizes);
	setCentralWidget(splitter);
	
	this->InitPlot(0);
	    
}

void MainWindow::InitPlot(int idx){
	current_plot_idx = idx;
	plot->clearPlottables();
	plot->replot();
	std::shared_ptr<BasePlot> ptr = plot_list[idx].create();

	refreshPreferences();
	ptr->validatePreferences(plotprefs);
	showHidePrefCombo();

	//std::tuple<double,stroke_type_t,period_type_t,int,int,int> plot_preferences = plotPreferences();

	//ptr->setPlotPreferences(plot_preferences);
	ptr->createPlot(plot);

	plot->replot();
}

MainWindow::~MainWindow()
{

}

void MainWindow::showHidePrefCombo(){
	bool allow; 
	allow = plotprefs.allow_period;
	if(allow){
		period_label->show();
		period_combo->show();
	}else{
		period_label->hide();
		period_combo->hide();
	}
	
	allow = plotprefs.allow_strokes;
	if(allow){
		styles_label->show();
		styles_combo->show();
	}else{
		styles_label->hide();
		styles_combo->hide();
	}
	
	allow = plotprefs.allow_pool;
	if(allow){
		pools_label->show();
		pools_combo->show();
	}else{
		pools_label->hide();
		pools_combo->hide();
	}
	
	allow = plotprefs.allow_interval;
	if(allow){
		interval_label->show();
		interval_combo->show();
	}else{
		interval_label->hide();
		interval_combo->hide();
	}
	
	allow = plotprefs.allow_points;
	if(allow){
		points_label->show();
		points_combo->show();
	}else{
		points_label->hide();
		points_combo->hide();
	}
	
	allow = plotprefs.allow_depth;
	if(allow){
		depth_label->show();
		depth_combo->show();
	}else{
		depth_label->hide();
		depth_combo->hide();
	}
	
}


