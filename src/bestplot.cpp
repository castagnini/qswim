
#include "testinput.h"
#include "bestplot.h"
#include "common_types.h"
#include "swimvolumes.h"
#include "qcustomplot.h"
#include "levmar.h"

const std::string BestGraph::graphLabel(){
	return label;
}

const std::string BestPlot::plotTitle(){
	return std::string("Best"); 
}

const std::string BestPlot::xAxisLabel(){
	return std::string("date"); 
}

const std::string BestPlot::yAxisLabel(){
	return std::string("min/100m"); 
}

void BestPlot::validatePreferences(PlotPreferences&prefs){
	if(ever)
	prefs.allow_period = false;
	else prefs.allow_period = true;
	prefs.allow_strokes = true;
	prefs.allow_pool = true;
	prefs.allow_interval = true;
	prefs.allow_points = false;
	prefs.allow_depth = false;	
	preferences = prefs;     
}

void BestPlot::createPlot(QCustomPlot* plot){
	double pool = preferences.pool;	
	period_type_t period = preferences.period;	
	stroke_type_t stroke = preferences.stroke;	
	int interval = preferences.interval;

	if (ever) period = EVER;
	SwimVolumes sv (pool,period, interval, stroke);

	if (sv.y.size()<1) return;
	std::shared_ptr<BestGraph> bestgraph = std::make_shared<BestGraph>();
	graphs.push_back(bestgraph);

	bestgraph->label = "best" ;
	bestgraph->x = sv.x;
	bestgraph->y = sv.y[0];
	bestgraph->calcMinMax();

	auto graph = this->graphs[0];
	// QCustomPlot needs a QVector.
	QVector<double> x= QVector<double>::fromStdVector(graph->x);
	QVector<double> y= QVector<double>::fromStdVector(graph->y);
	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(0)->setData(x, y);
	plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

	plot->graph(0)->setName(graph->graphLabel().c_str());

	//fit 
/*
	double par[5];
    int ret = levmar_fit(bestgraph->x.size(),&bestgraph->x[0],&bestgraph->y[0],par);
	x = QVector<double>(201) ;
	y = QVector<double>(201) ;
    for ( int i = 0 ; i< x.size(); i++) { 
		x[i] = bestgraph->x[0] + i*( bestgraph->x.back() - bestgraph->x[0])/200. ;
		double xx = x[i];
		y[i] = fit_func(par,0,&xx);
	}
    plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(1)->setPen(QPen(QColor(230.,43.0,23.,255), 2));
    plot->graph(1)->setData(x, y);
    plot->graph(1)->setName("fit");	

	x = QVector<double>({bestgraph->x[0], bestgraph->x.back()}) ;
	y = QVector<double>({par[0],par[0]}) ;
    plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(2)->setPen(QPen(QColor(17.,173.0,52.,0.9*255), 2));
    plot->graph(2)->setData(x, y);
    plot->graph(2)->setName("asintote");	

*/
	plot->yAxis2->setVisible(false);

	// Creates data from test input
	double minX,minY,maxX,maxY;
	minX = this->minX();
	minY = this->minY();
	maxX = this->maxX();
	maxY = this->maxY();

    // calc extra padding for the plots
    	plot->legend->setVisible(true);
    // give the axes some labels:
    plot->xAxis->setLabel(this->xAxisLabel().c_str());
    plot->yAxis->setLabel(this->yAxisLabel().c_str());
	plot->xAxis->setRangeReversed(false);
	plot->yAxis->setRangeReversed(false);

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);

	QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
  	plot->xAxis->setTicker(dateTimeTicker);
	dateTimeTicker->setDateTimeFormat("d. MMM\nyyyy");
  	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  	plot->yAxis->setTicker(timeTicker);
  
  	timeTicker->setTimeFormat("%m:%s");	
	
	plot->setInteraction(QCP::iRangeDrag, true);
	plot->setInteraction(QCP::iRangeZoom, true);
	
}






