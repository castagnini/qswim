
#ifndef PREFERENCES_H_
#define PREFERENCES_H_


class Preferences
{
    public:
        static Preferences& getInstance()
        {
            static Preferences    instance; // Guaranteed to be destroyed.
                                  // Instantiated on first use.
            return instance;
        }
		static bool correctOrderForIM(){return true;}
		static bool treatIMAsStyle () {return true;}
		static bool includeMixedInAnalysis () {return true;}



    private:
        Preferences() {}                    // Constructor? (the {} brackets) are needed here.


        // C++ 11
        // =======
        // We can use the better technique of deleting the methods
        // we don't want.
    public:
        Preferences(Preferences const&)               = delete;
        void operator=(Preferences const&)  = delete;

};

#endif


