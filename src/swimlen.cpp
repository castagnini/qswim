
#include "common_types.h"
#include "swimlen.h"
#include "preferences.h"

void SwimInterval::setIsIM(){
	
	is_im = false;
	if(!is_mixed || sublengths.size() == 0) return;	

    if (sublengths.size() % 4 != 0 ) return ;
    
	/* on 400IM n is 100 meters, on 100IM n is 25 meters and so on.. */
    long n = sublengths.size() /4 ;
    
    const stroke_type_t ims [4] = {BUTTERFLY, BACKSTROKE, BREASTSTROKE, FREESTYLE};
    
    if (Preferences::correctOrderForIM()) {
		int pos = 0 ; 
        for (int i = 0 ; i < 4 ; i++) {
            for (int j=0; j<n; j++) {
                if(sublengths[pos++].style != ims[i] ){
					is_im = false ;
					 return;
				}
            }
        }
		is_im=true;
		style = IM;
        return;
    }else{
        int done[4] = {-1,-1,-1,-1} ;
       	int pos = 0; 
        for (int i = 0 ; i < 4 ; i++) {
            stroke_type_t style_current = sublengths[pos++].style ;
            for (int j =0; j<i; j++) {
                if (done[j] == style_current) {
                    is_im = false;
					return;
                }
            }
            for (int j=1; j<n; j++) {
                if(sublengths[pos++].style != style_current){ 
					is_im = false;
					return ;
				}
            }
            done[i] = style_current;
        }
		is_im=true;
		style = IM;
        return;
    }
}









