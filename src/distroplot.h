

#ifndef _DISTROPLOT_H_
#define _DISTROPLOT_H_

#include <string>
#include <vector>
#include <memory>
#include "baseplot.h"


class DistroGraph : public BaseGraph {
	public:
		DistroGraph(): BaseGraph() { }
		const std::string graphLabel();

	std::string label;
};

class DistroPlot : public BasePlot {
	public:
		DistroPlot() ;
		const std::string plotTitle() ;
		const std::string xAxisLabel() ;
		const std::string yAxisLabel() ;

	void validatePreferences(PlotPreferences&);
	void createPlot(QCustomPlot* plot);
	double minX () {return ( graphs.size()>0 ? graphs[0]->minX : 0) ;}
	double maxX () {return ( graphs.size()>0 ? graphs[0]->maxX : 0) ;}
	double minY () {return ( graphs.size()>0 ? graphs[0]->minY : 0) ;}
	double maxY () {return ( graphs.size()>0 ? graphs[0]->maxY : 0) ;}
};


#endif


