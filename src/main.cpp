#include <iostream>
#include "activity.h"
#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	QTime t1 = QTime::currentTime();
	Activity::reloadActivities("../activities");
	QTime t2 = QTime::currentTime();
	std::cout<<"time passed: " << t1.msecsTo(t2)<< " msec"<<std::endl; 
    MainWindow w;
    w.show();

    return a.exec();
}
