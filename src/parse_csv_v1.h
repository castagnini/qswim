#ifndef _PARSE_CSV_V1_H_
#define _PARSE_CSV_V1_H_

#include <string>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "activity.h"

typedef struct {
	unsigned long filesize;
	unsigned long n_lines;
	unsigned long line_size;
	unsigned long tokens;
	// error info
} file_info_t, *file_info_p;

static inline off_t get_filesize(FILE*fp){
	fseeko(fp, 0, SEEK_END);
	off_t sz = ftello(fp);
	// rewind;
	fseeko(fp, 0, SEEK_SET);
	return sz;
}


// tokens is maxlen*total_tokens
char*read_file(char*filename, file_info_p info);

char* tokenize_line(char*line, char*tokens, int tok_size, int maxlen, int *found);


static inline int get_column_for_header_key (char * tokens,int line_size, int num_toks, char* key)
{
    int res = -1;
    for(int i =0 ; i < num_toks; i ++ ){
        if ( strcmp(key, &tokens[i*line_size])==0 ) {
            res = i;
            break;
        }
    }
    return res;
}


std::string parse_csv_v1(char*filename , Activity & act );

#endif


