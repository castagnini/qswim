#include <algorithm>
#include <ctime>
#include <cmath>
#include <vector>

#include "activity.h"
#include "common_types.h"
#include "distrovol.h"


DistroVol::DistroVol ( double pool_len ,period_type_t p, int interval_len,  stroke_type_t style, std::vector<volume_type_t> & quantities ){

	if(quantities.size() < 1) return;

	x.clear();
	y.clear();
	std::vector<Activity> activities = Activity::getActivities();

    std::map<double,double> empty_map; 
    for ( auto & act: activities ){
        for ( auto & ival : act.intervals ){
            empty_map[ival] = 0. ;
        } 
    }
	
	x.reserve(activities.size());
	y.reserve(quantities.size());

	struct tm timeinfo;
	memset(&timeinfo,0,sizeof(timeinfo));
    timeinfo.tm_hour = 0;
	timeinfo.tm_min = 30;
    timeinfo.tm_year = activities[0].year - 1900;
    timeinfo.tm_mon = activities[0].month - 1;
    timeinfo.tm_mday = activities[0].day;

    time_t time_origin = mktime ( &timeinfo );
	const double seconds_per_day = 60.*60.*24.;

		
	long old_x = -999999999;
	for(auto & act : activities){
		if( pool_len > 0. && fabs(act.pool_len - pool_len) > 0.1 ) continue;
		// -1 = ALL. I do not use ALL below, I use interval 
		double val = act.getValue(style, interval_len ,quantities[0]);
		if ( ! (val > 0.0) ) continue ;
		
		long days = act.days_from_origin; 
		switch (p)
		{
			case WEEKS:
			days -= act.day_of_week;
			break;
			case MONTHS:
			days -= act.day ;
			break;
			case YEARS:
			days -= act.day_of_year;
			case DAYS:
			default: break;
		}
		if( days != old_x ){
			x.push_back((double)time_origin + seconds_per_day * days);
		    y.push_back(empty_map);
			old_x = days;
		}
        std::map<double,double> & m = y.back() ;
        for ( auto & ival : act.intervals ){
            m[ival] += ival ;
        } 

	}
}


