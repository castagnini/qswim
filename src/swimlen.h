
#ifndef SWIMLEN_H
#define SWIMLEN_H

#include <vector>
#include "helpers.h"
#include "common_types.h"

class SwimLen {

public: 
	SwimLen(stroke_type_t sty, double m, double t, double strk, double c=0.)
		: style(sty), distance (m), time(t), strokes(strk), kcal(c)
	{
		is_drill = sty == DRILL ;	
		is_mixed = sty == MIXED ;	
		is_rest = sty == REST ;	
		pace = m > 0. ?  t*100./m : 0.;
		strokes_per_len = m > 0. ? strk*50./m : 0.;
		strokes_per_min = t > 0. ? strk*60./t : 0.;
	};
    
    bool is_mixed;
	bool is_drill;
    bool is_rest;

    stroke_type_t style;
    double distance;
    double time;
    double pace;
    double strokes;
    double kcal; 
	double strokes_per_len;
	double strokes_per_min;
};

/* An interval is a length made of N>=1 sublengths */
class SwimInterval: public SwimLen 
{
public: 
	SwimInterval(stroke_type_t sty, double m, double t, double strk, double c, int num_sublen=0): SwimLen(sty,m,t,strk,c){
		if(num_sublen>0) sublengths.reserve(num_sublen);
		rescaled_time = rescale_time(t,m);
	}
	// usefull for rest intervals
	SwimInterval(stroke_type_t sty,double t): SwimLen(sty,0.,t,0.,0.){};

	void appendSubLen(SwimLen & len){
		sublengths.push_back(len);
	}
   
    void setIsIM();
		
	std::vector<SwimLen> sublengths ;
    bool is_im;
    double rescaled_time;

};


#endif


