Version:1.0
start_time,total_distance,total_distance_units,total_timer_time,total_timer_time_units,total_cycles,total_calories,pool_length,pool_length_units
2015-11-10,1125.0,m,3322.813,s,395,209,25,m
split,swim_stroke,total_distance,total_distance_units,total_timer_time,total_timer_time_units,total_cycles,total_calories
Lap,BREASTSTROKE,25.0,m,24.273,s,10,7
1,BREASTSTROKE,25.0,m,23.687,s,10,
Rest,Rest,,,91.641,s,,
Lap,BACKSTROKE,50.0,m,53.695,s,21,10
1,BACKSTROKE,25.0,m,27.750,s,11,
2,BACKSTROKE,25.0,m,26.000,s,10,
Rest,Rest,,,166.930,s,,
Lap,BACKSTROKE,50.0,m,51.750,s,20,10
1,BACKSTROKE,25.0,m,27.250,s,11,
2,BACKSTROKE,25.0,m,24.500,s,9,
Rest,Rest,,,91.125,s,,
Lap,BACKSTROKE,50.0,m,48.297,s,22,10
1,BACKSTROKE,25.0,m,24.187,s,12,
2,BACKSTROKE,25.0,m,24.187,s,10,
Rest,Rest,,,206.516,s,,
Lap,BREASTSTROKE,50.0,m,50.758,s,26,14
1,BREASTSTROKE,25.0,m,26.812,s,13,
2,BREASTSTROKE,25.0,m,24.000,s,13,
Rest,Rest,,,92.742,s,,
Lap,DRILL,25.0,m,23.953,s,,
1,DRILL,25.0,m,23.953,s,0,
Rest,Rest,,,16.297,s,,
Lap,DRILL,25.0,m,25.160,s,,
1,DRILL,25.0,m,25.160,s,0,
Rest,Rest,,,44.109,s,,
Lap,DRILL,25.0,m,22.211,s,,
1,DRILL,25.0,m,22.211,s,0,
Rest,Rest,,,32.164,s,,
Lap,DRILL,25.0,m,22.336,s,,
1,DRILL,25.0,m,22.336,s,0,
Rest,Rest,,,367.852,s,,
Lap,DRILL,25.0,m,23.727,s,,
1,DRILL,25.0,m,23.727,s,0,
Rest,Rest,,,35.461,s,,
Lap,DRILL,25.0,m,23.148,s,,
1,DRILL,25.0,m,23.148,s,0,
Rest,Rest,,,110.227,s,,
Lap,BUTTERFLY,25.0,m,18.789,s,11,7
1,BUTTERFLY,25.0,m,18.750,s,11,
Rest,Rest,,,28.586,s,,
Lap,BUTTERFLY,25.0,m,19.469,s,8,7
1,BUTTERFLY,25.0,m,19.500,s,8,
Rest,Rest,,,62.219,s,,
Lap,BUTTERFLY,25.0,m,18.875,s,9,7
1,BUTTERFLY,25.0,m,18.875,s,9,
Rest,Rest,,,88.125,s,,
Lap,BUTTERFLY,25.0,m,18.000,s,10,7
1,BUTTERFLY,25.0,m,18.620,s,10,
Rest,Rest,,,98.938,s,,
Lap,FREESTYLE,100.0,m,85.609,s,35,20
1,FREESTYLE,25.0,m,20.875,s,11,
2,FREESTYLE,25.0,m,22.625,s,11,
3,FREESTYLE,25.0,m,22.620,s,6,
4,FREESTYLE,25.0,m,20.125,s,7,
Rest,Rest,,,188.516,s,,
Lap,FREESTYLE,50.0,m,38.852,s,19,10
1,FREESTYLE,25.0,m,19.937,s,9,
2,FREESTYLE,25.0,m,18.937,s,10,
Rest,Rest,,,94.211,s,,
Lap,FREESTYLE,50.0,m,38.844,s,20,10
1,FREESTYLE,25.0,m,19.937,s,11,
2,FREESTYLE,25.0,m,18.937,s,9,
Rest,Rest,,,90.656,s,,
Lap,FREESTYLE,100.0,m,92.172,s,50,20
1,FREESTYLE,25.0,m,21.312,s,11,
2,FREESTYLE,25.0,m,25.187,s,14,
3,FREESTYLE,25.0,m,24.937,s,14,
4,FREESTYLE,25.0,m,20.687,s,11,
Rest,Rest,,,98.766,s,,
Lap,FREESTYLE,25.0,m,19.813,s,10,5
1,FREESTYLE,25.0,m,19.812,s,10,
Rest,Rest,,,35.500,s,,
Lap,FREESTYLE,25.0,m,18.367,s,10,5
1,FREESTYLE,25.0,m,18.375,s,10,
Rest,Rest,,,25.633,s,,
Lap,FREESTYLE,25.0,m,18.289,s,10,5
1,FREESTYLE,25.0,m,18.375,s,10,
Rest,Rest,,,25.148,s,,
Lap,FREESTYLE,25.0,m,18.508,s,9,5
1,FREESTYLE,25.0,m,18.500,s,9,
Rest,Rest,,,25.180,s,,
Lap,FREESTYLE,25.0,m,18.656,s,9,5
1,FREESTYLE,25.0,m,18.687,s,9,
Rest,Rest,,,23.781,s,,
Lap,FREESTYLE,25.0,m,18.492,s,8,5
1,FREESTYLE,25.0,m,18.562,s,8,
Rest,Rest,,,24.258,s,,
Lap,FREESTYLE,25.0,m,18.445,s,8,5
1,FREESTYLE,25.0,m,18.500,s,8,
Rest,Rest,,,23.492,s,,
Lap,FREESTYLE,25.0,m,18.109,s,8,5
1,FREESTYLE,25.0,m,18.125,s,8,
Rest,Rest,,,23.328,s,,
Lap,FREESTYLE,25.0,m,18.586,s,10,5
1,FREESTYLE,25.0,m,18.625,s,10,
Rest,Rest,,,22.602,s,,
Lap,FREESTYLE,25.0,m,18.367,s,9,5
1,FREESTYLE,25.0,m,18.375,s,9,
Rest,Rest,,,81.633,s,,
Lap,FREESTYLE,50.0,m,41.109,s,22,10
1,FREESTYLE,25.0,m,22.000,s,12,
2,FREESTYLE,25.0,m,19.187,s,10,
Rest,Rest,,,38.780,s,,
Lap,FREESTYLE,50.0,m,41.477,s,21,10
1,FREESTYLE,25.0,m,19.937,s,10,
2,FREESTYLE,25.0,m,21.500,s,11,
