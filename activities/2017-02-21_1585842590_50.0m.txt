Version:1.0
start_time,total_distance,total_distance_units,total_timer_time,total_timer_time_units,total_cycles,total_calories,pool_length,pool_length_units
2017-02-21,2000.0,m,2853.656,s,143,58,50,m
split,swim_stroke,total_distance,total_distance_units,total_timer_time,total_timer_time_units,total_cycles,total_calories
Lap,DRILL,400.0,m,365.547,s,,
1,DRILL,50.0,m,45.693,s,0,
2,DRILL,50.0,m,45.693,s,0,
3,DRILL,50.0,m,45.693,s,0,
4,DRILL,50.0,m,45.693,s,0,
5,DRILL,50.0,m,45.693,s,0,
6,DRILL,50.0,m,45.693,s,0,
7,DRILL,50.0,m,45.693,s,0,
8,DRILL,50.0,m,45.693,s,0,
Rest,Rest,,,54.289,s,,
Lap,DRILL,100.0,m,95.453,s,,
1,DRILL,50.0,m,47.726,s,0,
2,DRILL,50.0,m,47.726,s,0,
Rest,Rest,,,93.172,s,,
Lap,DRILL,300.0,m,283.984,s,,
1,DRILL,50.0,m,47.330,s,0,
2,DRILL,50.0,m,47.330,s,0,
3,DRILL,50.0,m,47.330,s,0,
4,DRILL,50.0,m,47.330,s,0,
5,DRILL,50.0,m,47.330,s,0,
6,DRILL,50.0,m,47.330,s,0,
Rest,Rest,,,132.266,s,,
Lap,DRILL,200.0,m,196.742,s,,
1,DRILL,50.0,m,49.185,s,0,
2,DRILL,50.0,m,49.185,s,0,
3,DRILL,50.0,m,49.185,s,0,
4,DRILL,50.0,m,49.185,s,0,
Rest,Rest,,,78.445,s,,
Lap,BACKSTROKE,50.0,m,63.148,s,24,11
1,BACKSTROKE,50.0,m,63.187,s,24,
Rest,Rest,,,27.477,s,,
Lap,BACKSTROKE,50.0,m,64.141,s,24,11
1,BACKSTROKE,50.0,m,64.125,s,24,
Rest,Rest,,,22.922,s,,
Lap,FREESTYLE,100.0,m,85.656,s,47,18
1,FREESTYLE,50.0,m,43.620,s,24,
2,FREESTYLE,50.0,m,42.562,s,23,
Rest,Rest,,,35.781,s,,
Lap,DRILL,100.0,m,94.320,s,,
1,DRILL,50.0,m,47.160,s,0,
2,DRILL,50.0,m,47.160,s,0,
Rest,Rest,,,177.550,s,,
Lap,DRILL,400.0,m,392.133,s,,
1,DRILL,50.0,m,49.160,s,0,
2,DRILL,50.0,m,49.160,s,0,
3,DRILL,50.0,m,49.160,s,0,
4,DRILL,50.0,m,49.160,s,0,
5,DRILL,50.0,m,49.160,s,0,
6,DRILL,50.0,m,49.160,s,0,
7,DRILL,50.0,m,49.160,s,0,
8,DRILL,50.0,m,49.160,s,0,
Rest,Rest,,,168.805,s,,
Lap,DRILL,200.0,m,185.633,s,,
1,DRILL,50.0,m,46.408,s,0,
2,DRILL,50.0,m,46.408,s,0,
3,DRILL,50.0,m,46.408,s,0,
4,DRILL,50.0,m,46.408,s,0,
Rest,Rest,,,152.180,s,,
Lap,FREESTYLE,100.0,m,84.180,s,48,18
1,FREESTYLE,50.0,m,41.312,s,23,
2,FREESTYLE,50.0,m,42.875,s,25,
